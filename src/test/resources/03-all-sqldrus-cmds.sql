connect test-ifx

create table tmptst(
	id 				integer not null,
	name			varchar(50) not null,
	version			integer default 1 not null ,
	date_created	datetime year to second default current year to second
	);
alter table tmptst add constraint primary key (id) constraint tmptst_pk;
create sequence tmptst_seq;
create view tmptst_v as select * from tmptst;
create synonym tmptst_syn for tmptst;

CREATE FUNCTION tmptst_func(p_ver integer) returning integer;
 return p_ver+1;
end function;

create trigger tmptst_trig update on tmptst
	REFERENCING OLD AS pre NEW AS post
	for each row (execute function tmptst_func(pre.version) into tmptst.version);

setvar maxid integer 9

insert into tmptst(id,name) values (9,"nine");
insert into tmptst(id,name) values (10,"ten");

select id, name from tmptst where id = :maxid;

setenv verbose rows                                                                  
ls tmptst*
lt tmptst*
lv tmptst*
lq tmptst*
lx tmptst*

describe tmptst
constraints tmptst
trigger tmptst
src tmptst_func

rollback;

disconnect
