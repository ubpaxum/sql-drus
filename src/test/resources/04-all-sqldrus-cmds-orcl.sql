connect test-orcl

create user tmpusr identified by tmptst default tablespace users;
grant connect, resource, create view to tmpusr;

create table tmpusr.tmptst(
	id 				integer not null,
	name			varchar(50) not null,
	version			integer default 1 not null ,
	date_created	date default sysdate
	);
alter table tmpusr.tmptst add constraint tmptst_pk primary key (id);
create sequence tmpusr.tmptst_seq start with 1;
create view tmpusr.tmptst_v as select * from tmptst;
create synonym tmpusr.tmptst_syn for tmptst;

CREATE OR REPLACE FUNCTION tmpusr.tmptst_func(p_ver integer) returns integer as
begin
 return p_ver+1;
end function;
/

create OR REPLACE trigger tmpusr.tmptst_trig 
	before update on tmpusr.tmptst
	REFERENCING OLD AS pre NEW AS post for each row
begin
	:new.version := tmpusr.tmptst_func(:old.id);
end;
/

setvar maxid integer 9

insert into tmpusr.tmptst(id,name) values (9,'nine');
insert into tmpusr.tmptst(id,name) values (10,'ten');

select id, name from tmpusr.tmptst where id = :maxid;

setenv verbose rows                                                                  
ls tmpusr.tmptst*
lt tmpusr.tmptst*
lv tmpusr.tmptst*
lq tmpusr.tmptst*
lx tmpusr.tmptst*

describe tmpusr.tmptst
constraints tmpusr.tmptst
trigger tmpusr.tmptst
src tmpusr.tmptst_func

rollback;

drop user tmpusr cascade;

disconnect
