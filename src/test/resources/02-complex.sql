connect test-ifx

create table tmptst(
	id 				integer not null,
	name			varchar(50) not null,
	date_created	datetime year to second default current year to second
	);
setenv verbose rows
describe tmptst
setenv verbose all

insert into tmptst(id,name) values (1,"one");
insert into tmptst(id,name) values (2,"two");
insert into tmptst(id,name) values (3,"three");
insert into tmptst(id,name) values (4,"four");
insert into tmptst(id,name) values (5,"five");
insert into tmptst(id,name) values (6,"six");
insert into tmptst(id,name) values (7,"seven");
insert into tmptst(id,name) values (8,"eight");
insert into tmptst(id,name) values (9,"nine");
insert into tmptst(id,name) values (10,"ten");

select id, name from tmptst where id < 10;

rollback;

disconnect
