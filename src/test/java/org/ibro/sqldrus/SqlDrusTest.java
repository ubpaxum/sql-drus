package org.ibro.sqldrus;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 1/11/17
 * Time: 1:58 PM
 * Short description of purpose.
 */
import org.ibro.sqldrus.util.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SqlDrusTest {
    private String sqlFile;
    private String outFile;
//    static private final Logger log = LoggerFactory.getLogger(SqlDrusTest.class);

    public SqlDrusTest(String sqlFile, String outFile){
        this.sqlFile = sqlFile;
        this.outFile = outFile;
    }

    @Test
    public void sqlTest() throws Exception{
        try (
                InputStream sqlStream = new FileInputStream(sqlFile);
                InputStream outStream = new FileInputStream(outFile);
                ByteArrayOutputStream outBytes = new ByteArrayOutputStream(2048);
                PrintStream out = new PrintStream( outBytes );
        ) {
            SQLDrus drus = new SQLDrus(sqlStream, out);
            Logger log = LoggerFactory.getLogger(SqlDrusTest.class);
            drus.setVerbose(Verbose.FOOTERS);
            drus.start();
//            drus.readLoop();
            drus.stop();

            String outExpected = Utils.readAll(Utils.readUTF8(outStream));
            String outActual   = Utils.readAll( new InputStreamReader( new ByteArrayInputStream(outBytes.toByteArray()) ) );

            Pattern ptrn = Pattern.compile(outExpected);

            if ( ! ptrn.matcher(outActual).matches() ){
                log.debug("Actual:\n"+outActual);
                log.warn(String.format("%s: NOK", sqlFile));
                TestUtils.logDiff(log, outExpected, outActual);
                assertTrue("OK", false);
            } else {
                log.info(String.format("%s: OK", sqlFile) );
                assertTrue("OK", true);
            }
        }

    }


    @Parameterized.Parameters(name="{0}")
    public static Iterable<Object[]> generateParameters() throws IOException {
        System.out.println( System.getProperty("user.dir") );
        return TestUtils.findMessageTuples("./src/test/resources", ".sql", ".out");
    }

}
