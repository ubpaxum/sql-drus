package org.ibro.sqldrus;

import org.ibro.sqldrus.util.Utils;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 1/11/17
 * Time: 2:15 PM
 * Short description of purpose.
 */
public class TestUtils {

    private TestUtils() {}

    public static List<Object[]> findFiles(String dir, String suffix)
            throws IOException {

        List<Object[]> result = new ArrayList<>();

        for (String filename : Utils.findFiles(dir, suffix)) {
            result.add(new Object[] { filename });
        }
        return result;
    }

    public static List<Object[]> findMessageTuples(String subdir, String primaryExt, String secondaryExt)
            throws IOException {

        List<Object[]> result = new ArrayList<>();
        List<String> primaryFiles = Utils.findFiles(subdir, primaryExt);
        for (String primary : primaryFiles) {
            if (secondaryExt != null) {
                String base = primary.substring(0, primary.length() - primaryExt.length());
                String secondary = base + secondaryExt;
                result.add(new Object[]{primary, secondary});
            }
            else {
                result.add(new Object[]{primary});
            }
        }

        return result;
    }

    public static void logDiff(Logger log, String a, String b) {
        int diffIndex = Utils.indexOfDifference(a, b);
        int n1 = a.indexOf('\n',diffIndex);
        int n2 = b.indexOf('\n',diffIndex);
        if ( diffIndex >= 0 && n1 >= 0 && n2 >= 0 )
            log.debug(String.format("diff starts at position: %s\n===>\n---%s\n+++%s\n<===",
                    diffIndex,
                    a.substring( Utils.startLineIndex(a, diffIndex), n1),
                    b.substring( Utils.startLineIndex(b, diffIndex), n2) ));
    }

}
