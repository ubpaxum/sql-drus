package org.ibro.sqldrus.command;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.dialect.SqlDialect;

import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/25/16
 * Time: 4:11 PM
 * Short description of purpose.
 */
public interface SQLCommands {
    public static final char PAD_CHARACTER = '-';
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    boolean hasConnection();

    SqlDialect getDialect();

    void connectionLoad();

    void error(Exception e) ;

    void error(Exception e, String message);

    void error(String message);

    void output(String line);

    void output(String line, Verbose level);

    void setVerboseOutput(Verbose verbose);

    void setVerboseOutputString(String level);

    void setOnError(String action);

    String getOnError();

    Verbose getVerboseOutput();

    String getVerboseOutputString();

    void setOutputStream (PrintStream newStream);

    PrintStream getOutputStream();

    String getConnectionName();

    void doSQLCommand(String command);

    void connect(String connInfo, String userName, String password, boolean keepCurrent);

    void disconnectAll();

    void disconnect(String connName);

    void setVariable(String varName, String dataType, String value) throws ParseException;

    void printVariables();

    void showConnections();

    Map<String,Object> getBindVariables();

    void switchConnection(String connName);

    List<String> getAutocompleteObjects();

    List<String> getConnectionNames();

    void setMaxColSize(int maxColSize);

    int getMaxColSize();

    void setBulkSize(int bulkSize);

    int getBulkSize();

}
