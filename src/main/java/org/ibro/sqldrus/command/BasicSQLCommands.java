package org.ibro.sqldrus.command;

import org.ibro.sqldrus.SqlConstants;
import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.config.SqlDrusConfig;
import org.ibro.sqldrus.dialect.*;
import org.ibro.sqldrus.interceptor.BulkStatementInterceptor;
import org.ibro.sqldrus.interceptor.FetchInterceptor;
import org.ibro.sqldrus.interceptor.StatementInterceptor;
import org.sag.commons.security.Cipherer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BasicSQLCommands implements SQLCommands {

    private static final Logger log = LoggerFactory.getLogger(BasicSQLCommands.class);
    private static final Pattern interceptPtrn = Pattern.compile("\\bintercept\\s+([A-Za-z0-9_-]+)\\b");

    private Map<String,SqlDialect>  connectionMap;


    protected PrintStream out ;
    protected Verbose verbosity ;
    protected Properties connectionDef ;
    protected List<String> connectionNames;
    protected String currConnName = null;
    protected Map<String,Object> bindVariables;
    protected SqlDrusConfig config;
    protected int maxColSize = SqlConstants.MAX_COL_SIZE;
    protected String onError = "CONTINUE";

    private SqlDialect dialect ;
    private int dbCounter = 0;
    private int bulkSize = SqlConstants.BULK_SIZE;

    public BasicSQLCommands(PrintStream pwOut, SqlDrusConfig config) {
        this(pwOut,config,Verbose.ALL);

    }

    public BasicSQLCommands(PrintStream pwOut, SqlDrusConfig config, Verbose verbose){
        out = pwOut;
        verbosity = verbose;
        this.config = config;
        connectionLoad();
        bindVariables = new HashMap<>();
        connectionMap = new HashMap<>();
    }


    @Override
    public SqlDialect getDialect() {
        return dialect;
    }

    @Override
    public boolean hasConnection() {
        return dialect != null && dialect.getConnection() != null;
    }

    @Override
    public void connectionLoad(){
        connectionNames = new ArrayList<>();
        if (connectionDef == null){
            connectionDef = new Properties();
        } else {
            connectionDef.clear();
        }
        for ( String fName: config.getConnectionFiles() ){
            File connFile = new File(fName);
            if (connFile.exists()){
                output("Loading connections from "+fName, Verbose.ALL);
                try {
                    FileInputStream fioConn = new FileInputStream(connFile) ;
                    connectionDef.load( fioConn );
                    fioConn.close();
//                    output("Connection definitions loaded.",Verbose.ALL.level());
                } catch (FileNotFoundException e){
                    error(e, "Connection file not found: "+connFile.getAbsolutePath() );
                } catch (IOException e){
                    error(e, "Connection file can not be closed: "+connFile.getAbsolutePath());
                }
          }
        }
        connectionNames.addAll( connectionDef.stringPropertyNames() );
        if ( connectionDef.isEmpty() ){
            output("No connections defined. Using jdbc URLs.",Verbose.ALL);
        }
    }

    @Override
    public void error(Exception e){
        error(e, null);
    }

    @Override
    public void error(Exception e, String message){
        if ( message == null && e != null )  message = e.getMessage();
        if ( e !=null ){
            if ( e instanceof SQLException ) {
                SQLException se = (SQLException) e;
                log.error("Error: "+se.getErrorCode(),se);
                output(""+se.getErrorCode()+":"+se.getMessage(), Verbose.ERROR);
                SQLException ex = se;
                while ((ex = ex.getNextException() ) != null ){
                    log.error("Error: "+ex.getErrorCode()+":"+ex.getMessage(),ex);
                    output(""+ex.getErrorCode()+":"+ex.getMessage(), Verbose.ERROR);
                }
            } else {
                log.error("Unexpected exception", e );
            }
        }
        log.error(  message, e );
        output(message, Verbose.ERROR);
        if ( getOnError().equalsIgnoreCase("exit") ){
            System.exit(-1);
        } else if (getOnError().equalsIgnoreCase("rollback")) {
            if (dialect!=null) {
                dialect.rollback();
                output("Transaction rollbacked.", Verbose.ERROR);
            }
        } else if (getOnError().equalsIgnoreCase("commit")) {
            if (dialect!=null) {
                dialect.commit();
                output("Transaction commited.", Verbose.ERROR);
            }
        }
    }

    @Override
    public void error(String message){
        error(null,message);
    }

    @Override
    public void output(String line){
        out.println(line);
    }

    @Override
    public void output(String line, Verbose level){
        if (level.level() <= verbosity.level())
            out.println(line);
    }

    @Override
    public void setVerboseOutput(Verbose verbose){
        verbosity = verbose;
    }

    @Override
    public void setVerboseOutputString(String level){
        verbosity = Verbose.valueOf(level.toUpperCase());
    }

    @Override
    public Verbose getVerboseOutput(){
        return verbosity;
    }

    @Override
    public String getVerboseOutputString(){
        return verbosity.name();
    }

    @Override
    public void setOutputStream(PrintStream newStream){
        if (out != newStream ){
            out.flush();
//            out.close();
        }
        out = newStream;
    }

    @Override
    public PrintStream getOutputStream(){
        return out;
    }

    @Override
    public String getConnectionName(){
        return currConnName;
    }

    @Override
    public void doSQLCommand(String command){
        Matcher m = interceptPtrn.matcher(command);
        int begin = 0;
        int end;
        if ( m.find() ){
            Stack<InterceptionHolder> holder = new Stack<>();
            holder.push( new InterceptionHolder(dialect, command.substring(begin,m.start())) );

            while (true){
                String connName = m.group(1);
                SqlDialect dialect1 = connName.equalsIgnoreCase("this")? dialect : connectionMap.get(connName);
                end = m.end() + 1;
                if ( dialect1 == null ){
                    error("Connection "+connName+" is not active. Use CONN <connName> to activate it.");
                    return;
                }
                if ( m.find() ){
                    begin = end;
                    String sql = command.substring(begin, m.start() );
                    holder.push( new InterceptionHolder(dialect1,sql) );
                } else {
                    String sql = command.substring(end);
                    holder.push( new InterceptionHolder(dialect1,sql) );
                    break;
                }
            }
            FetchInterceptor interceptor = holder.pop().createInterceptor();
            while( holder.size() > 1 ){
                interceptor = holder.pop().createInterceptor(interceptor);
            }
            InterceptionHolder first = holder.pop();
            first.dialect.doSQLCommand( first.sql, interceptor );

//            String sql1 = command.substring(begin, m.start() );
//            String sql2 = command.substring(m.end()+1);
//            String connName = m.group(1);
//            SqlDialect dialect1 = connectionMap.get(connName);
//            if ( dialect1 == null ){
//                output("Connection "+connName+" is not active. Use CONN <connName> to activate it.", Verbose.ERROR.level());
//                return;
//            }
//            dialect.doSQLCommand(sql1, new StatementInterceptor(this, dialect1, sql2) );
        } else {
            dialect.doSQLCommand(command);
        }
    }

    public void connect(String connInfo, String userName, String password, boolean keepCurrent){
        if ( !keepCurrent && currConnName != null ){
            disconnect(currConnName,true);
        }

        String connString = connectionDef.getProperty(connInfo);
        if ( connString == null ) {
            connString = connInfo;
            connInfo = "db"+(dbCounter++);
        }
        // decrypt possibly encrypted password
        connString = Cipherer.getInstance().decryptAllIfEncrypted(connString);
        if ( userName != null ) userName = Cipherer.getInstance().decryptAllIfEncrypted(userName);
        if ( password != null ) password = Cipherer.getInstance().decryptAllIfEncrypted(password);

//        log.debug("connect: {} ({}/{})", connString, userName, password);
        if ( connString.startsWith("jdbc:informix") ){
            dialect = new InformixDialect(this);
            dialect.connect(connString,userName,password);
        } else if ( connString.startsWith("jdbc:oracle") ){
            dialect = new OracleDialect(this);
            dialect.connect(connString,userName,password);
        } else if ( connString.startsWith("jdbc:db2") ){
            dialect = new DB2Dialect(this);
            dialect.connect(connString,userName,password);
        } else if ( connString.startsWith("jdbc:postgresql") ){
            dialect = new PostgresDialect(this);
            dialect.connect(connString,userName,password);
        }  else {
            error("Unknown connection driver " + connString);
        }
        if ( dialect != null && dialect.getConnection() != null ){
            currConnName = connInfo;
            connectionMap.put(currConnName, dialect);
        }
    }

    @Override
    public void disconnectAll() {
        Iterator<String> i = connectionMap.keySet().iterator();
        while ( i.hasNext() ) {
            disconnect(i.next(),false);
            i.remove();
        }
    }

    @Override
    public void disconnect(String connName){
        disconnect(connName, true);
        // if active connection is disconnected
        // switch to first active connection in map if any
        if ( !connectionMap.isEmpty() ){
            currConnName = connectionMap.keySet().iterator().next();
            dialect = connectionMap.get(currConnName);
        }
    }

    private void disconnect(String connName, boolean remove){
        if ( connName == null ) connName = currConnName;
        SqlDialect dialect1 = connectionMap.get(connName);
        if ( dialect1 != null ){
            dialect1.disconnect();
            if (remove) connectionMap.remove(connName);
        }
        if ( connName.equals(currConnName) ){
            currConnName = null;
            dialect = null;
        }
    }


    @Override
    public void setVariable(String varName, String dataType, String value) throws ParseException{
        Object val = castToType( value, dataType);
        bindVariables.put(varName, val);
    }

    @Override
    public void printVariables(){
        output( "Bind variables" );
        for ( String varName: bindVariables.keySet()){
            Object val = bindVariables.get(varName);
            String strVal = val==null?"NULL":val.toString();
            if ( val instanceof Date) strVal = dateFormat.format((Date)val);
            output( String.format("\t%s(%s)=%s",varName,val==null?"null":val.getClass().getSimpleName(), strVal) );
        }
    }

    private Object castToType(String value, String dataType) throws ParseException {
        if ( value == null ) return null;
        log.debug(String.format("Cast '%s' to type %s",value,dataType) );
        switch (dataType) {
            case "VARCHAR":
                return value;
            case "INTEGER":
                return new Long(value.trim());
            case "SMALLINT":
                return new Integer(value.trim());
            case "NUMERIC":
                return new BigDecimal(value.trim());
            case "DATE":
                return new java.sql.Date(dateFormat.parse(value).getTime());
            default:
                throw new RuntimeException("Unknown datatype " + dataType);
        }
    }


    @Override
    public Map<String,Object> getBindVariables(){
        return bindVariables;
    }

    @Override
    public void showConnections() {
        for ( String connName: connectionMap.keySet() ){
            String info = connectionDef.getProperty(connName);
            output( String.format("%s %s: %s", connName.equals(currConnName)?"*":" ", connName,info) );
        }
    }

    @Override
    public void switchConnection(String connName){
        if ( connName == null ) return;
        SqlDialect dialect1 = connectionMap.get(connName);
        if ( dialect1 != null && !connName.equals(currConnName) ){
            this.dialect  = dialect1;
            this.currConnName  = connName;
        }
    }

    @Override
    public List<String> getAutocompleteObjects(){
        if ( !hasConnection() ) return null;
        return dialect.getAutocompleteObjects();
    }

    final class InterceptionHolder{
        public final SqlDialect dialect;
        public final String sql;
        public final boolean isBulk;

        public InterceptionHolder(SqlDialect dialect, String sql) {
            this.dialect = dialect;
            this.isBulk = sql.matches("^bulk .*");
            this.sql = isBulk?sql.replaceFirst("^bulk ",""): sql;
        }

        public FetchInterceptor createInterceptor(){
           return isBulk? new BulkStatementInterceptor(BasicSQLCommands.this, dialect, sql):
                          new StatementInterceptor(BasicSQLCommands.this,dialect,sql);
        }

        public FetchInterceptor createInterceptor(FetchInterceptor interceptor){
            return isBulk? new BulkStatementInterceptor(BasicSQLCommands.this,dialect,sql,interceptor):
                           new StatementInterceptor(BasicSQLCommands.this,dialect,sql,interceptor);
        }

    }

    @Override
    public List<String> getConnectionNames() {
        return connectionNames;
    }


    @Override
    public int getMaxColSize() {
        return maxColSize;
    }

    @Override
    public void setMaxColSize(int maxColSize) {
        this.maxColSize = maxColSize;
    }

    @Override
    public String getOnError() {
        return onError;
    }

    @Override
    public void setOnError(String onError) {
        this.onError = onError;
    }

    @Override
    public int getBulkSize() {
        return bulkSize;
    }

    @Override
    public void setBulkSize(int bulkSize) {
        this.bulkSize = bulkSize;
    }
}
