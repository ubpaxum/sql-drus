package org.ibro.sqldrus.util;

import java.io.*;
import java.sql.Types;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/28/16
 * Time: 9:52 AM
 * Short description of purpose.
 */
public class StringUtils {
    public static String padString( String s, int n, char padChar, boolean rightPadding ) {
        if (n <= 0) return "";
        if ( s == null ) s="";
        StringBuffer str ;
        int strLength  = s.length();
        if ( n > strLength ) {
            str = new StringBuffer(s);
            if (rightPadding ){
                for ( int i = strLength; i < n ; i ++ )  str.append( padChar );
            } else {
                for ( int i = strLength; i < n ; i ++ )  str.insert(0, padChar );
            }
        } else {
            str = new StringBuffer(s.substring(0,n));
        }
        return str.toString();
    }

    public static String trimString( String s, int n) {
        if (n <= 0) return "";
        else if (s.length() <= n ) return s;
        return s.substring(0, n );
    }

    public static String rtrim( String s, int n) {
        if ( n <=0 || s==null || s.length()==0 )  return "";
        int l = s.length();
        int i = n>l?l-1:n-1;
        while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
            i--;
        }
        return s.substring(0,i+1);
    }


    public static String rtrim(String s) {
        int i = s.length()-1;
        while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
            i--;
        }
        return s.substring(0,i+1);
    }

    public static boolean rightPadding( int sqlType ){
        return (    sqlType != Types.INTEGER &&
                sqlType != Types.SMALLINT &&
                sqlType != Types.NUMERIC &&
                sqlType != Types.DECIMAL &&
                sqlType != Types.DOUBLE &&
                sqlType != Types.FLOAT &&
                sqlType != Types.REAL );
    }


    public static String streamToString(InputStream stream) throws IOException {
        return new String(streamToByteArray(stream), "UTF-8");
    }

    public static byte[] streamToByteArray(InputStream stream) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];
        int n =stream.read(buffer);
        while( n>0 ){
            bos.write(buffer, 0, n);
            n =stream.read(buffer);
        }
        return bos.toByteArray();
    }

    public static ByteArrayInputStream toByteStream(InputStream stream) throws IOException {
        return new ByteArrayInputStream( streamToByteArray(stream) );
    }

    public static void in2out(InputStream in, OutputStream out, int bufferLength) throws IOException {
        byte[] buffer = new byte[bufferLength];
        int bytes_read;
        while ( (bytes_read=in.read(buffer)) != -1 ){
            out.write(buffer,0,bytes_read);
            out.flush();
        }
    }
    public static void in2out(InputStream in, OutputStream out) throws IOException {
        in2out(in,out,2048);
    }


}
