package org.ibro.sqldrus.config;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/9/16
 * Time: 11:18 AM
 * Short description of purpose.
 */
public class SqlDrusConfig {
    public static final String CONNECTION_FILE  = "connection.properties";
    public static final String STARTUP_FILE     = "sql-drus.startup";
    public static final String KEYWORDS_FILE     = "sql-drus.keywords";
    public static final String SYNTAX_FILE     = "sql-drus.syntax";
    public static final String HISTORY_FILE      = ".sql-drus.history";

    public static final String SQLDRUS_HOME="SQLDRUS_HOME";

    private Set<String> configDirs ;
    private String      sqlDrusHome = null;


    public SqlDrusConfig(){
        determineSqlDrusHome();
        File logDir =  new File(sqlDrusHome+"/log");
        if ( !logDir.exists() ) logDir.mkdirs();

        configDirs = new LinkedHashSet<String>();
        configDirs.add(sqlDrusHome);
        configDirs.add( System.getProperty("user.home") ); // home of the user
        configDirs.add( System.getProperty("user.dir") );  // current working directory
    }

    public List<String> getStartupFiles(){
        return getFileList(STARTUP_FILE);
    }

    public List<String> getConnectionFiles(){
        return getFileList(CONNECTION_FILE);
    }
    public List<String> getKeywordsFiles(){
        return getFileList(KEYWORDS_FILE);
    }

    public String getHistoryFile(){
        return System.getProperty("user.home")+"/"+HISTORY_FILE;
    }

    private List<String> getFileList(String fileName) {
        List<String> files = new ArrayList<String>();
        for ( String dir: configDirs ){
            File directory = new File(dir);
            if ( directory.exists() && directory.canRead()  ){
                File file = new File(dir,fileName);
                if ( file.exists() && file.canRead() )
                    files.add( file.getAbsolutePath() );
            }
        }
        return files;
    }

    public String getSqlDrusHome(){
        return sqlDrusHome;

    }

    private void determineSqlDrusHome() {
        sqlDrusHome = System.getProperty(SQLDRUS_HOME);
        if ( sqlDrusHome == null || "".equals(sqlDrusHome) ){
            sqlDrusHome = System.getenv().get(SQLDRUS_HOME);

        }
        if ( sqlDrusHome == null || "".equals(sqlDrusHome) ){
            final File f = new File(SqlDrusConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            sqlDrusHome = extractMainPath(f.getAbsolutePath());
        }

        System.setProperty(SQLDRUS_HOME,sqlDrusHome);
    }

    private String extractMainPath(String path){
        if ( path.endsWith(".jar") ){
            Pattern p = Pattern.compile("^([\\\\A-Za-z0-9_/:. -]+)(sql-drus-[0-9a-z_.-]+\\.jar)",Pattern.UNICODE_CASE+Pattern.MULTILINE);
            Matcher m = p.matcher(path);
            if ( m.find() ){
                String pth = m.group(1);
                if ( pth.endsWith("/lib/") || pth.endsWith("\\lib\\") ) pth = pth.substring(0, pth.length()-5 );
                return pth;
            }
        }
        return path;
    }

}
