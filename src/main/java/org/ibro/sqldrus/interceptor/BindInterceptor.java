package org.ibro.sqldrus.interceptor;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import static org.ibro.sqldrus.util.StringUtils.*;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:32 PM
 * Short description of purpose.
 */
public class BindInterceptor extends PrintInterceptor {
    private List<String> vars;
    public BindInterceptor(SQLCommands cmd, List<String> vars){
        super(cmd);
        this.vars = vars;
    }

    @Override
    public int processRow(ResultSet rs) throws SQLException {
        if ( rsMeta == null )  initMetaData(rs.getMetaData());
        Object val ;
        String line = "";
        for (int i=0; i<columnCount; i++){
            // do not pad last column
            String colName = rsMeta.getColumnName(i+1);
            boolean isLastColumn =  (i == columnCount-1);
            if ( columnsType[i] == Types.CLOB ) val = rs.getClob(i+1);
            else val = rs.getObject(i+1);
            String sVal = cmd.getDialect().valueToString( columnsType[i], val );
            line += isLastColumn?rtrim(sVal):
                    padString(sVal,columnsSize[i],' ', rightPadding(columnsType[i]) )+" ";
            String varName = vars.stream().filter(x-> x.equalsIgnoreCase(colName)).findFirst().orElse(null);
            if (  varName!=null  ) cmd.getBindVariables().put(varName,val);
        }
        cmd.output(line,Verbose.ROWS);
        return 1;
    }

}
