package org.ibro.sqldrus.interceptor;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import static org.ibro.sqldrus.util.StringUtils.*;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:32 PM
 * Short description of purpose.
 */
public abstract class BaseIterceptor implements FetchInterceptor {
    private int batchSize = 100;
    private int rowsProcessed = 0;

    public BaseIterceptor(){
    }

    @Override
    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    @Override
    public int getRowsProcessed() {
        return rowsProcessed;
    }

    @Override
    public void setRowsProcessed(int rowsProcessed) {
        this.rowsProcessed = rowsProcessed;
    }
}
