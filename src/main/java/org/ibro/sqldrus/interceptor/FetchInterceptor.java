package org.ibro.sqldrus.interceptor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:29 PM
 * Short description of purpose.
 */
public interface FetchInterceptor {
    static final Pattern bindParamExtractor = Pattern.compile("[^:]:\\w+\\b", Pattern.UNICODE_CASE + Pattern.MULTILINE);

    int processRow(ResultSet rs) throws SQLException;

    void release();

    int getRowsProcessed();

    void setRowsProcessed(int rowsProcessed);

    int getBatchSize();

    void setBatchSize(int batchSize);

}
