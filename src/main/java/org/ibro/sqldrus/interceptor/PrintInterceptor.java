package org.ibro.sqldrus.interceptor;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import static org.ibro.sqldrus.util.StringUtils.*;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:32 PM
 * Short description of purpose.
 */
public class PrintInterceptor extends BaseIterceptor {
    protected ResultSetMetaData  rsMeta;
    protected SQLCommands cmd;
    protected int [] columnsSize;
    protected int [] columnsType;
    protected int columnCount;

    public PrintInterceptor(SQLCommands cmd){
        this.cmd = cmd;
    }

    protected void initMetaData(ResultSetMetaData rsMeta) throws SQLException{
        this.rsMeta = rsMeta;
        String heading = "";
        String colLabel = null;
        int colSize = 0;
        columnCount = rsMeta.getColumnCount();

        columnsSize = new int[columnCount];
        columnsType = new int[columnCount];
        for (int i=0; i< columnCount; i++){
            colLabel = rsMeta.getColumnName(i+1);
            colSize = rsMeta.getColumnDisplaySize(i+1);
            if (colSize==0) colSize=10;
            else if ( colSize > cmd.getMaxColSize() ) colSize = cmd.getMaxColSize();

            if (colSize > colLabel.length() ) {
                heading = heading + padString( colLabel, colSize, ' ', true)+" ";
            } else {
                heading = heading + colLabel.substring(0,colSize)+" ";;
            }
            columnsSize[i] = colSize;
            columnsType[i] = rsMeta.getColumnType(i+1);
        }
        cmd.output(heading.trim(), Verbose.HEADINGS);
        cmd.output(padString("", heading.length(), cmd.PAD_CHARACTER, true), Verbose.HEADINGS );
    }

    @Override
    public int processRow(ResultSet rs) throws SQLException {
        if ( rsMeta == null )  initMetaData(rs.getMetaData());
        Object val ;
        String line = "";
        for (int i=0; i<columnCount; i++){
            // do not pad last column
            boolean isLastColumn =  (i == columnCount-1);
            if ( columnsType[i] == Types.CLOB ) val = rs.getClob(i+1);
            else val = rs.getObject(i+1);
            String sVal = cmd.getDialect().valueToString( columnsType[i], val );
            line += isLastColumn?rtrim(sVal):
                    padString(sVal,columnsSize[i],' ', rightPadding(columnsType[i]) )+" ";

        }
        cmd.output(line,Verbose.ROWS);
        return 1;
    }

    @Override
    public void release() {
        rsMeta = null;
    }
}
