package org.ibro.sqldrus.interceptor;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:32 PM
 * Short description of purpose.
 */
public class DumpInterceptor extends BaseIterceptor {
    private ResultSetMetaData  rsMeta;
    private SQLCommands cmd;
    private String tableName;
    private String [] columnsName;
    private int [] columnsType;
    private int columnCount;

    public DumpInterceptor(SQLCommands cmd, String tableName){
        this.cmd = cmd;
        this.tableName=tableName;
    }

    private void initMetaData(ResultSetMetaData rsMeta) throws SQLException{
        this.rsMeta = rsMeta;
        columnCount = rsMeta.getColumnCount();

        columnsName = new String[columnCount];
        columnsType = new int[columnCount];
        for (int i=0; i< columnCount; i++){
            columnsName[i] = rsMeta.getColumnName(i+1);
            columnsType[i] = rsMeta.getColumnType(i+1);
        }
    }

    @Override
    public int processRow(ResultSet rs) throws SQLException {
        if ( rsMeta == null )  initMetaData(rs.getMetaData());
        Object val ;
        StringBuilder columns = new StringBuilder( "insert into "+tableName+"(");
        StringBuilder values  = new StringBuilder( "\n   values (");
        for (int i=0; i<columnCount; i++){
            // do not pad last column
            boolean isLastColumn =  (i == columnCount-1);
            columns.append(columnsName[i]);
            columns.append(isLastColumn ? ")" : ", ");

            values.append( cmd.getDialect().repr(rs.getObject(i + 1), columnsType[i]) );
            values.append(isLastColumn ? ");" : ", ");

        }
        cmd.output( columns.toString() + values.toString(),Verbose.ROWS);
        return 1;
    }

    @Override
    public void release() {
        rsMeta = null;
    }

}
