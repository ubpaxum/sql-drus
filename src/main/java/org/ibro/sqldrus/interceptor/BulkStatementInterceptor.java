package org.ibro.sqldrus.interceptor;

import org.ibro.sqldrus.command.SQLCommands;
import org.ibro.sqldrus.dialect.SqlDialect;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/14/16
 * Time: 6:04 PM
 * Short description of purpose.
 */
public class BulkStatementInterceptor extends BaseIterceptor {
//    private static final Logger log = LoggerFactory.getLogger(StatementInterceptor.class);

    private SQLCommands sqlCmd;
    private String sql ;
    private SqlDialect dialect;
    private PreparedStatement ps ;
    private List<String> namedParameters;
    private FetchInterceptor interceptor;


    public BulkStatementInterceptor(SQLCommands sqlCmd, SqlDialect dialect, String sql) {
        this( sqlCmd, dialect, sql, new PrintInterceptor(sqlCmd)  );
    }

    public BulkStatementInterceptor(SQLCommands sqlCmd, SqlDialect dialect, String sql, FetchInterceptor interceptor) {
        this.sqlCmd = sqlCmd;
        this.dialect = dialect;
        this.sql = sql;
        this.interceptor = interceptor;
        interceptor.setBatchSize( sqlCmd.getBulkSize() );
    }

    @Override
    public int processRow(ResultSet rs) throws SQLException {
        if ( ps == null  ){
//            log.debug("Interceptor SQL: "+sql);
            ps = dialect.getConnection().prepareStatement( extractNamedParameters(sql) );
            setRowsProcessed(0);
        }
        List<Object> params = new ArrayList<Object>();
        for ( String varName: namedParameters ){
            if ( varName.matches("\\d+") ){
                int idx = Integer.parseInt(varName);
                params.add( rs.getObject(idx) );
            } else {
                params.add( sqlCmd.getBindVariables().get(varName) );
            }
        }
        int i =1;
        for (Object param: params){
            ps.setObject(i++, param);
        }
        ps.addBatch();
        setRowsProcessed(  getRowsProcessed() + 1 );
        if ( (getRowsProcessed() % getBatchSize()) == 0   ){
            ps.executeBatch();
            if (!dialect.getConnection().getAutoCommit()) dialect.getConnection().commit();
        }
        return 1;
    }

    private String extractNamedParameters(String sql){
        namedParameters = new ArrayList<String>();

        StringBuffer sb = new StringBuffer( sql.length() );
        Matcher m = bindParamExtractor.matcher(sql);
        while ( m.find() ){
            String varName = m.group().substring(2);
            namedParameters.add(varName);
            m.appendReplacement(sb, m.group().substring(0,1)+"?");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    @Override
    public void release() {
        try {
            if ((getRowsProcessed() % getBatchSize()) != 0) {
                ps.executeBatch();
                if (!dialect.getConnection().getAutoCommit()) dialect.getConnection().commit();
            }
        } catch (SQLException e){
            throw new RuntimeException("Cannot execute last batch",e);
        }
        try { if (ps!=null) ps.close(); } catch (SQLException ignore) {}
        interceptor.release();
    }
}
