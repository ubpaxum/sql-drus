package org.ibro.sqldrus.interceptor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/13/16
 * Time: 4:32 PM
 * Short description of purpose.
 */
public class ListInterceptor<E> extends BaseIterceptor {
    private List<E> result;

    public ListInterceptor(List<E> result){
        this.result = result;
    }


    @Override
    public int processRow(ResultSet rs) throws SQLException {
        result.add( (E) rs.getObject(1) );
        return 1;
    }

    @Override
    public void release() {
    }
}
