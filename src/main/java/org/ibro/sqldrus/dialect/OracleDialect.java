package org.ibro.sqldrus.dialect;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.ibro.sqldrus.dialect.ObjectTypes.*;
import static org.ibro.sqldrus.dialect.OracleSqlConstants.*;
import static org.ibro.sqldrus.util.StringUtils.padString;
import static org.ibro.sqldrus.util.StringUtils.rtrim;

public class OracleDialect extends AbstractSqlDialect {

    private static final Logger log = LoggerFactory.getLogger(OracleDialect.class);
    private static Map<ObjectTypes,String> tabTypeMap;

    static {
        tabTypeMap = new HashMap<ObjectTypes,String>();
        tabTypeMap.put(ALL,"*");
        tabTypeMap.put(TABLE,"TABLE");
        tabTypeMap.put(VIEW,"VIEW");
        tabTypeMap.put(SEQUENCE,"SEQUENCE");
        tabTypeMap.put(SYNONYM,"SYNONYM");
        tabTypeMap.put(PROCEDURE,"PROCEDURE|FUNCTION|TRIGGER|PACKAGE|PACKAGE BODY");
    }

    public OracleDialect(SQLCommands sqlCmd){
        super(sqlCmd, DIALECT_NAME, JDBC_DRIVER_NAME);
        setDateFormat( new SimpleDateFormat("yyyy-MM-dd") );
        setDatetimeFormat( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") );
    }


    private String getTypeName( String type, int scale, Number precision, String nullable){
        String isNull = ( "N".equals(nullable)) ? "NOT NULL":"NULL";
        String typeName = null;
        if ( "CHAR".equals(type) )  typeName = "CHAR("+scale+")";
        else if ("VARCHAR".equals(type) ) typeName = "VARCHAR("+scale+")";
        else if ("VARCHAR2".equals(type) ) typeName = "VARCHAR("+scale+")";
        else if ("NUMBER".equals(type) ) typeName = "NUMBER("+scale+(precision!=null && precision.intValue()>0?","+precision:"")+")";
        else if ("DATE".equals(type) ) typeName = "DATE";
        else  typeName=type+(scale>0?"("+scale+")":"");

        return padString(typeName, 20, ' ',true)+padString(isNull,8,' ', false);
    }

    private void describeTableOrView(String type, String owner, String name){
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            log.debug( String.format("SQL: %s, owner: %s, name: %s", SQL_TAB_COLUMNS, owner, name) );
            ps = conn.prepareStatement(SQL_TAB_COLUMNS);
            ps.setString(1,owner);
            ps.setString(2, name);
            rs = ps.executeQuery();
            sqlCmd.output("COLUMN NAME                   TYPE                NULL      DEFAULT",Verbose.HEADINGS);
            sqlCmd.output("-------------------------------------------------------------------",Verbose.HEADINGS);
            String def_value = null;
            int idx = 0;
            while ( rs.next() ){
                def_value = rs.getString(6);
                sqlCmd.output(rtrim(padString(rs.getString(1), 30, ' ', true) + getTypeName(rs.getString(2), rs.getInt(3), (Number) rs.getObject(4), rs.getString(5)) + " " + padString(def_value, 15, ' ', true)),Verbose.ROWS);
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally{
            try{
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }
    }

    @Override
    public void describe(String objName){
        String owner = getObjectOwner(objName);
        String objectName = getObjectName(objName);
        String likeName = toLikePattern(objectName);
        String line = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_TABLES);
            ps.setString(1, likeName);
            ps.setString(2, owner);
            rs = ps.executeQuery();
            while ( rs.next() ){
                String objectType = rs.getString(1);
                String name = rs.getString(2);
                String objectOwner = rs.getString(3);

                line = objectType+": "+objectOwner+"."+name /*+" Created: "+rs.getString(5)*/;
                sqlCmd.output(line, Verbose.HEADINGS);
                if (objectType.equals("TABLE") || objectType.equals("VIEW")){
                    sqlCmd.output( padString("",line.length(),sqlCmd.PAD_CHARACTER, true), Verbose.HEADINGS );
                    describeTableOrView(objectType,  objectOwner, name );
                }
                sqlCmd.output("", Verbose.ALL);
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally{
            try {
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }

    }

    @Override
    public void describeIndexes(String tabName){
        String owner = getObjectOwner(tabName);
        String objectName = getObjectName(tabName);
        String line = "";
        PreparedStatement ps = null;
        PreparedStatement psCols = null;
        ResultSet rs = null;
        ResultSet rsCols;

        try {
            psCols = conn.prepareStatement(SQL_COLUMN);

            ps = conn.prepareStatement(SQL_INDEX_DESCRIBE);
            ps.setString(1, objectName );
            ps.setString(2, owner );

            line = "INDEXES of "+objectName;
            sqlCmd.output(line, Verbose.ALL);
            sqlCmd.output("INDEX NAME                    TYPE                 UNIQUE   ROWS  COLUMNS", Verbose.HEADINGS);
            sqlCmd.output("----------------------------------------------------------------------------------------", Verbose.HEADINGS);
            rs = ps.executeQuery();
            while ( rs.next() ){
                String idxName  = rs.getString(1);
                String idxOwner  = rs.getString(2);
                String idxType  = rs.getString(3);
                String idxUniq  = rs.getString(4);
                int    rows     = rs.getInt(5);

                line = padString(idxName,30,' ',true)+padString(idxType,20,' ',true)+padString(idxUniq,8,' ',false)+padString(""+rows,8,' ',false);

                psCols.setString(1, idxOwner);
                psCols.setString(2, idxName);
                rsCols = psCols.executeQuery();


                while (  rsCols.next()  ){
                    line += " "+rsCols.getString(1).trim();
                }
                rsCols.close();
                sqlCmd.output(rtrim(line), Verbose.ROWS);
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally {
            try {
                if (psCols != null ) psCols.close();
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }

    }

    @Override
    public void sourceProcedure(String procName){
        String objName= getObjectName(procName);
        String objOwner = getObjectOwner(procName);
        source(SQL_PROC_SOURCE, objOwner + "." + objName);
        sqlCmd.output("/");
    }

    @Override
    public void sourceTrigger(String trigName){
        String objName= getObjectName(trigName);
        String objOwner = getObjectOwner(trigName);
        source(SQL_TRIGGER, objOwner + "." + objName);
    }

    public void listObjectByType(ObjectTypes type, String name){
        String objectType = tabTypeMap.get(type);
        String objName= getObjectName(name);
        String objOwner = getObjectOwner(name);
        String likeName = toLikePattern(objName);
        doSQLCommand(SQL_USER_TABLES, new Object[]{objOwner, objectType, likeName});
    }

    public void describeConstraints(String tabName){
        String objName = getObjectName(tabName);
        String objOwner = getObjectOwner(tabName);
        Verbose oldVerbose = sqlCmd.getVerboseOutput();
        sqlCmd.setVerboseOutput(Verbose.ROWS);
        sqlCmd.output("\nPrimary and unique constraints");
        doSQLCommand(SQL_PK_CONSTRAINTS, new Object[]{objName, objOwner});
//        sqlCmd.output("\nCheck constraints");
//        sqlCmd.doSQLCommand(SQL_CHECK_CONSTAINTS, new Object[]{objName, objOwner});
        sqlCmd.output("\nReferences");
        doSQLCommand(SQL_REF_CONSTRAINTS, new Object[]{objName, objOwner});
        sqlCmd.output("\nReferenced by");
        doSQLCommand(SQL_REF_BY_CONSTRAINTS, new Object[]{objName, objOwner});
        sqlCmd.setVerboseOutput(oldVerbose);
    }

    private String toLikePattern(String name){
        if ( name == null ) return "%";
        if ( name.contains("*") ) return name.replace("_", "\\_").replace('*', '%');
        return name;
    }


    @Override
    public String getObjectName(String objName) {
        String name=super.getObjectName(objName);
        return name==null?null:name.toUpperCase();
    }

    @Override
    public String getObjectOwner(String objName) {
        return  super.getObjectOwner(objName).toUpperCase();
    }

    @Override
    public String getCurrentUser() {
        return super.getCurrentUser().toUpperCase();
    }

    @Override
    public List<String> getAutocompleteObjects() {
        return resultAsList( SQL_AUTOCOMPLETE_OBJECTS );
    }

    @Override
    public String valueToString(int dataType, Object val) {
//        if ( val != null && val instanceof oracle.sql.TIMESTAMP ){
//            return  ((oracle.sql.TIMESTAMP)val).stringValue();
//        }
        return super.valueToString(dataType, val);
    }

    @Override
    public String repr(Object val, int colType) {
        if ( val != null && val instanceof Date ){
            return String.format("to_date('%s','YYYY-MM-DD HH24:MI:SS')", getDatetimeFormat().format((Date) val)  );
        }
        return super.repr(val, colType);
    }

    @Override
    public List<String> getColumns(String tableName) {
        String owner = getObjectOwner(tableName);
        String objectName = getObjectName(tableName);
        return getColumns(owner,objectName, SQL_COLUMNS );
    }
}
