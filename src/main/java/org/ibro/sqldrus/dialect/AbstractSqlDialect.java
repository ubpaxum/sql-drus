package org.ibro.sqldrus.dialect;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;
import org.ibro.sqldrus.interceptor.*;
import org.ibro.sqldrus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

public abstract class AbstractSqlDialect implements SqlDialect {
    private static final Logger log = LoggerFactory.getLogger(AbstractSqlDialect.class);
    private DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private DateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    protected Connection conn;
    protected String     uri;
    protected String     dialectName;
    protected String     jdbcDriverClass;
    protected boolean    autoCommit;
    protected String     currentUser;
    protected String     userName;
    protected String     password;
    protected SQLCommands sqlCmd;
    protected List<Object> sqlParameters;
    protected boolean      doBindings = true;

    public AbstractSqlDialect(SQLCommands sqlCmd,String dialectName, String jdbcDriverClass) {
        this.dialectName = dialectName;
        this.jdbcDriverClass = jdbcDriverClass;
        this.sqlCmd = sqlCmd;
    }

    public Connection getConnection(){
        return conn;
    }

    public String getDialectName(){
        return dialectName;
    }

    public void connect(String connInfo, String userName, String password){
        if (conn != null) disconnect();
        try {
            Class.forName(jdbcDriverClass).newInstance();
            if (userName == null )
                conn = DriverManager.getConnection(connInfo);
            else
                conn = DriverManager.getConnection(connInfo,userName, password);
            conn.setAutoCommit(autoCommit);
            currentUser = conn.getMetaData().getUserName();
            uri = connInfo;
            this.userName = userName;
            this.password = password;
            sqlCmd.output("Connected.", Verbose.ERROR);
            log.info(String.format("Connected to %s as %s",uri.replaceFirst("password=[^;$]+","password=****"),currentUser));
        } catch (SQLException e){
            sqlCmd.error(e);
        } catch (Exception e){
            sqlCmd.error(e);
        }

    }


    protected List<String> getColumns(String owner, String tableName, String sql) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> columns = new ArrayList<>();

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1,owner);
            ps.setString(2, tableName);
            rs = ps.executeQuery();
            while ( rs.next() ){
                columns.add( rs.getString(1) );
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally{
            try{
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }
        return columns;
    }

    public void disconnect(){
        if (conn == null ){
            sqlCmd.output("Not connected.", Verbose.ERROR);
        }else {
            // ignore sql exception on close connection
            try { conn.close(); } catch (SQLException e) {}
            log.info(String.format("Disconnected from %s as %s",uri.replaceFirst("password=[^;$]+","password=****"),currentUser));
            sqlCmd.output("Disconnected.",Verbose.ERROR);
            conn = null;
            currentUser = null;
            uri = null;
            userName = null;
            password = null;
        }
    }

    abstract public void describe(String objName);

    abstract public void describeIndexes(String tabName);

    abstract public void sourceProcedure(String procName);

    abstract public void sourceTrigger(String trigName);

    abstract public void describeConstraints(String tabName);

    public String getObjectName(String objName){
        if ( objName == null ) return null;
        objName = objName.replaceAll(";","");
        int idx = objName.indexOf('.');
        if ( idx < 0 ) return objName;
        else return objName.substring(idx + 1);
    }

    public String getObjectOwner(String objName){
        if ( objName == null ) return getCurrentUser();
        int idx = objName.indexOf('.');
        if ( idx < 0 ) return getCurrentUser();
        else return objName.substring(0, idx);
    }

    public void doSQLCommand(String command){
        String sql = mangleNamedParameters(command);
        doSQLCommand(sql, sqlParameters.toArray() );
    }

    @Override
    public void doSQLCommand(String command, FetchInterceptor interceptor){
        String sql = mangleNamedParameters(command);
        doSQLCommand(sql, sqlParameters.toArray(), interceptor );
    }

    public void doSQLCommand(String command, Object[] params){
        doSQLCommand(command, params, new PrintInterceptor(sqlCmd) );
    }

    @Override
    public void dumpTable(String tableName, String command) {
        log.info("dump "+tableName);
        String sql = mangleNamedParameters(command);
        doSQLCommand(sql, sqlParameters.toArray(), new DumpInterceptor(sqlCmd,tableName));
    }

    @Override
    public void bind(List<String> vars, String command) {
        log.info("bind "+vars);
        vars.forEach(v-> sqlCmd.getBindVariables().put(v,null) );
        String sql = mangleNamedParameters(command);
        doSQLCommand(sql, sqlParameters.toArray(), new BindInterceptor(sqlCmd,vars));
    }

    @Override
    public int execute(PreparedStatement ps, Object[] params, FetchInterceptor interceptor) throws SQLException {
        int rowsProcessed = 0;
        if ( params != null && params.length>0 ){
            ps.clearParameters();
            for (int i=0; i<params.length; i++){
                ps.setObject(i + 1, params[i]);
            }
        }
        if (interceptor instanceof BulkStatementInterceptor) ps.setFetchSize( interceptor.getBatchSize() );
        boolean hasResultSet = ps.execute();
        if ( hasResultSet ){
            ResultSet rs = ps.getResultSet();
            while ( rs.next() ) rowsProcessed+=interceptor.processRow(rs);
            rs.close();
        } else {
            rowsProcessed = ps.getUpdateCount();
        }
        return rowsProcessed;
    }

    public void doSQLCommand(String command, Object[] params, FetchInterceptor interceptor){
        try {
            if ( conn.isClosed()  ) connect(uri,userName,password);
            if ( !conn.isValid(2)  ) connect(uri,userName,password);
        } catch (SQLException e) {
            log.error("Error", e);
        }
        PreparedStatement ps =  null;
        Statement stmt = null;
        ResultSet rs = null;
        boolean hasResultSet = false;
        int rowsProcessed = 0;

        StringTokenizer st = new StringTokenizer(command);
        String cmd1 = st.nextToken().toLowerCase();
        boolean isDML = false;
        // handle SELECT ... INTO TABLE as DML if
        isDML =  cmd1.equals("insert") || cmd1.equals("update") || cmd1.equals("delete");

        try{
            log.debug("Parsing: "+command);
            if ( !doBindings ){
                stmt = conn.createStatement();
                stmt.execute(command);
                stmt.close();
            }  else {
                ps = conn.prepareStatement(command);
                if ( params != null && params.length>0 ){
//                    log.debug("params: "+params);
                    for (int i=0; i<params.length; i++){
                        ps.setObject(i + 1, params[i]);
                    }
                }
                hasResultSet = ps.execute();
                if ( hasResultSet ){
                    rs = ps.getResultSet();
                    while ( rs.next() ) rowsProcessed+= interceptor.processRow(rs);
                    interceptor.release();
                    rs.close();
                } else {
                    rowsProcessed = ps.getUpdateCount();
                }
            }
            if (hasResultSet){
                log.debug("Rows fetched: "+rowsProcessed);
                sqlCmd.output("\n" + rowsProcessed + " row(s) selected.", Verbose.FOOTERS);
                // do nothing - fetch method has done it already
            } else if ( isDML ){
                log.debug("Rows processed: " + rowsProcessed);
                sqlCmd.output(rowsProcessed + " rows " + cmd1 + "`d.", Verbose.FOOTERS);
            } else {
                log.debug("Result: OK");
                sqlCmd.output(cmd1 + "`d.", Verbose.FOOTERS);
            }
        } catch (SQLException e){
            log.debug("Result: ERROR");
            sqlCmd.error(e);
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }

    }

    public void listTables(String name){
        listObjectByType(ObjectTypes.TABLE,name);
    }

    public void listViews(String name){
        listObjectByType(ObjectTypes.VIEW,name);
    }

    public void listSequences(String name){
        listObjectByType(ObjectTypes.SEQUENCE,name);
    }

    public void listSynonyms(String name){
        listObjectByType(ObjectTypes.SYNONYM,name);
    }

    public void listProcedures(String name){
        listObjectByType(ObjectTypes.PROCEDURE,name);
    }

    abstract public void listObjectByType(ObjectTypes type, String name);

    abstract public List<String> getAutocompleteObjects();

    public void source(String sql, String name){
        Verbose verbose = sqlCmd.getVerboseOutput();
        sqlCmd.setVerboseOutput(Verbose.ROWS);
        String objName= getObjectName(name);
        String objOwner = getObjectOwner(name);
        doSQLCommand(sql, new Object[]{objName, objOwner});
        sqlCmd.setVerboseOutput(verbose);
    }

    public void setAutoCommit(String pOnOff){
        if ( conn == null ){
            sqlCmd.output("Not connected.",Verbose.ERROR);
            return;
        }
        try {
            boolean pAutoCommit = pOnOff.equalsIgnoreCase("ON");
            if (pAutoCommit != autoCommit) conn.setAutoCommit( pAutoCommit );
            autoCommit = pAutoCommit;
        } catch (SQLException e) {
            log.error("Error", e);
        }
        sqlCmd.output("Auto commit set to/is " + (autoCommit?"ON":"OFF"), Verbose.ALL );
    }

    public String getAutoCommit(){
        return autoCommit?"ON":"OFF";
    }


    protected String mangleNamedParameters(String sql){
        sqlParameters = new ArrayList<Object>();
        if ( !doBindings ) return sql;

        StringBuffer sb = new StringBuffer( sql.length() );
        Matcher m = bindParamExtractor.matcher(sql);
        while ( m.find() ){
            String varName = m.group().substring(2);
            if ( sqlCmd.getBindVariables().containsKey(varName) ){
                m.appendReplacement(sb, m.group().substring(0,1)+"?");
                sqlParameters.add(sqlCmd.getBindVariables().get(varName));
            } else {
                // just skip it. probably database name is referred
                m.appendReplacement(sb,m.group() );
            }
        }
        m.appendTail(sb);
//        log.debug("mangled sql: " + sb.toString());
//        log.debug("params: " + sqlParameters);
        return sb.toString();
    }

    @Override
    public String getCurrentUser() {
        return currentUser;
    }


    @Override
    public boolean isDoBindings() {
        return doBindings;
    }

    @Override
    public void setDoBindings(boolean doBindings) {
        this.doBindings = doBindings;
    }

    @Override
    public String getUri(){
        return uri;
    }

    protected List<String> resultAsList(String sql){
        List<String> res = new ArrayList<String>();
        ListInterceptor<String> interceptor = new ListInterceptor<String>( res  );
        Verbose verbose = sqlCmd.getVerboseOutput();
        sqlCmd.setVerboseOutput(Verbose.SILENT);
        doSQLCommand(sql,interceptor);
        sqlCmd.setVerboseOutput(verbose);
        return res;
    }


    @Override
    public String valueToString(int dataType, Object val) {
        String sVal;
        if ( val == null ) {
            sVal = "(null)";
        } else if ( val instanceof Clob) {
            StringBuilder sb = new StringBuilder();
            try (Reader reader = ((Clob) val).getCharacterStream()) {
                BufferedReader br = new BufferedReader(reader);
                String line;
                while(null != (line = br.readLine())) {
                    sb.append(line);
                }
                br.close();
                sVal = sb.toString();
            } catch (SQLException | IOException e){
                log.error("Cannot read CLOB data",e);
                sVal = "--- ERROR --";
            }
        } else if ( val instanceof Blob) {
            try {
                sVal = StringUtils.streamToString(((Blob) val).getBinaryStream());
            } catch (Exception e) {
                sVal = "--- ERROR --";
                LoggerFactory.getLogger(PrintInterceptor.class).error("Can not fetch BLOB value", e);
            }
        } else {
            sVal = val.toString();
        }
        return sVal;
    }

    @Override
    public DateFormat getDateFormat() {
        return dateFormat;
    }

    @Override
    public DateFormat getDatetimeFormat() {
        return datetimeFormat;
    }

    @Override
    public void commit() {
        if (conn!=null) try {conn.commit();} catch (SQLException ignore) {}
    }

    @Override
    public void rollback() {
        if (conn!=null) try {conn.rollback();} catch (SQLException ignore) {}
    }

    protected void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    protected void setDatetimeFormat(DateFormat datetimeFormat) {
        this.datetimeFormat = datetimeFormat;
    }

    public String repr(Object val, int colType){
        if ( val == null )  return "null";
        if ( val instanceof String )  return "'"+ escape((String)val) +"'";
        if ( val instanceof java.util.Date && colType == Types.DATE)  return "'"+ dateFormat.format((java.util.Date) val) +"'";
        if ( val instanceof java.util.Date && colType == Types.TIMESTAMP)  return "'"+ datetimeFormat.format( (java.util.Date) val ) +"'";

        return String.format("%s",val);
    }

    protected String escape(String s ){
        return s.replace("'","''");
    }

}
