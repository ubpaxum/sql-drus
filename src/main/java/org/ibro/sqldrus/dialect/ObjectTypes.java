package org.ibro.sqldrus.dialect;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/14/16
 * Time: 11:11 AM
 * Short description of purpose.
 */
public enum ObjectTypes {
    ALL("%"),
    TABLE("T"),
    VIEW("V"),
    SEQUENCE("Q"),
    SYNONYM("S"),
    PROCEDURE("X");

    private String code;
    ObjectTypes (String code){
        this.code=code;
    }

    public String code(){
        return code;
    }

    public static ObjectTypes fromCode(String code){
        for ( ObjectTypes x: ObjectTypes.values() ){
            if ( x.code().equals(code) ) return x;
        }
        return null;
    }
}