package org.ibro.sqldrus.dialect;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/25/16
 * Time: 5:11 PM
 * Short description of purpose.
 */
public interface PostgresSqlConstants {
      String DIALECT_NAME = "Postgres";
      String JDBC_DRIVER_NAME = "org.postgresql.Driver";

      String SQL_USER_TABLES = "select table_name::varchar(50) as name, table_type::varchar(20) as type  " +
            "from information_schema.tables " +
            "where table_schema=? and table_type ~ ? and table_name like ? ESCAPE '\\'" +
            "order by 1,2";

      String SQL_PROC_SOURCE = "select nvl(decode(line,1,'CREATE OR REPLACE ','')||replace(text,chr(10),''),' ') as txt  " +
            "from  all_source where name = ? and owner =? order by type, line";

      String SQL_TAB_COLUMNS = "select column_name, data_type, coalesce(character_maximum_length, numeric_precision), numeric_scale, is_nullable, column_default " +
            "from information_schema.columns " +
            "where table_schema=? and table_name=? order by ordinal_position";

      String SQL_COLUMNS = "SELECT column_name "+
            "from information_schema.columns " +
            "where table_schema=? and table_name=? order by ordinal_position";

      String SQL_PK_CONSTRAINTS = "select tc.constraint_name::varchar(35) as constraint_name, ctu.column_name::varchar(35) as  column_name " +
            "from information_schema.table_constraints tc, " +
            "     information_schema.constraint_column_usage ctu " +
            "where tc.table_name=? and tc.table_schema=? " +
            "   and tc.constraint_name=ctu.constraint_name and tc.constraint_schema=ctu.constraint_schema and tc.constraint_type='PRIMARY KEY'";

      String SQL_REF_CONSTRAINTS = "select rkcu.table_name::varchar(35) as ref_table, " +
            "    tc.constraint_name::varchar(35) as fk_constrant, " +
            "    rc.unique_constraint_name::varchar(35) as ref_constraint, " +
            "    pkcu.column_name::varchar(35) as fk_column, " +
            "    rkcu.column_name::varchar(35) as pk_column " +
            " from information_schema.table_constraints tc " +
            "      join information_schema.key_column_usage pkcu  " +
            "       on tc.constraint_name=pkcu.constraint_name and pkcu.constraint_schema=tc.constraint_schema " +
            "      join information_schema.referential_constraints rc " +
            "       on tc.constraint_name=rc.constraint_name and rc.constraint_schema=tc.constraint_schema " +
            "      join information_schema.key_column_usage rkcu  " +
            "       on rc.unique_constraint_name=rkcu.constraint_name and rkcu.constraint_schema=rc.unique_constraint_schema and pkcu.ordinal_position=rkcu.ordinal_position " +
            "where tc.table_name=? and tc.table_schema=? and tc.constraint_type='FOREIGN KEY' " +
            "order by tc.constraint_name, pkcu.ordinal_position";
      String SQL_REF_BY_CONSTRAINTS = "select pkcu.table_name::varchar(35) as ref_table,  " +
            "    tc.constraint_name::varchar(35) as fk_constrant,  " +
            "    rc.unique_constraint_name::varchar(35) as ref_constraint,  " +
            "    pkcu.column_name::varchar(35) as fk_column,  " +
            "    rkcu.column_name::varchar(35) as pk_column  " +
            " from information_schema.table_constraints tc  " +
            "      join information_schema.key_column_usage pkcu   " +
            "       on tc.constraint_name=pkcu.constraint_name and pkcu.constraint_schema=tc.constraint_schema  " +
            "      join information_schema.referential_constraints rc  " +
            "       on tc.constraint_name=rc.constraint_name and rc.constraint_schema=tc.constraint_schema  " +
            "      join information_schema.key_column_usage rkcu   " +
            "       on rc.unique_constraint_name=rkcu.constraint_name and rkcu.constraint_schema=rc.unique_constraint_schema and pkcu.ordinal_position=rkcu.ordinal_position  " +
            "where rkcu.table_name=? and rkcu.table_schema=? and tc.constraint_type='FOREIGN KEY' " +
            "order by tc.constraint_name, pkcu.ordinal_position";

    String SQL_CHECK_CONSTAINTS = "select cc.constraint_name::varchar(35), cc.check_clause::varchar(100) " +
            "from information_schema.table_constraints tc " +
            "     join information_schema.check_constraints cc " +
            "       on tc.constraint_name=cc.constraint_name and cc.constraint_schema=tc.constraint_schema " +
            "where tc.table_name=? and tc.table_schema=? and tc.constraint_type='CHECK' and tc.constraint_name not like '%_not_null' " +
            "order by tc.constraint_name";
      String SQL_INDEX_DESCRIBE = "select indexname, indexdef " +
            "from pg_catalog.pg_indexes where tablename=? and schemaname=?";

      String SQL_TABLES = "select table_type, table_name, table_schema from information_schema.tables where table_name like ? and table_schema=? order by table_name, table_type";

      String SQL_TRIGGER = "select nvl(decode(s.line,1,'CREATE OR REPLACE ', '')||replace(s.text,chr(10),'')||" +
            "decode(s.line,t.nrows,chr(10)||'/',''),' ') as txt " +
            "    from  all_source s, " +
            "      (select x1.owner, x1.name, max(line) as nrows " +
            "         from all_source x1, all_triggers x2 " +
            "         where x2.table_name=? and x2.table_owner=? and x2.owner=x1.owner and x2.trigger_name=x1.name and x1.type='TRIGGER' " +
            "         group by x1.owner, x1.name) t" +
            "    where  t.owner=s.owner and t.name=s.name and s.type='TRIGGER' " +
            "    order by s.name, s.line";

      String SQL_AUTOCOMPLETE_OBJECTS =  "select table_name from information_schema.tables order by table_name";
}