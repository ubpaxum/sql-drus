package org.ibro.sqldrus.dialect;

import org.ibro.sqldrus.interceptor.FetchInterceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public interface SqlDialect {
    static final Pattern bindParamExtractor = Pattern.compile("[^:]:\\w+\\b", Pattern.UNICODE_CASE + Pattern.MULTILINE);

    static final List<String> SUPPORTED_DIALECT_NAMES = Arrays.asList(InformixSqlConstants.DIALECT_NAME, OracleSqlConstants.DIALECT_NAME, DB2SqlConstants.DIALECT_NAME);

    Connection getConnection();

    String getDialectName();

    void connect(String connInfo, String userName, String password);

    void disconnect();

    void describe(String objName);

    void describeIndexes(String tabName);

    void sourceProcedure(String procName);

    void sourceTrigger(String trigName);

    void describeConstraints(String tabName);

    String getObjectName(String objName);

    String getObjectOwner(String objName);

    void doSQLCommand(String command);

    void doSQLCommand(String command, FetchInterceptor interceptor);

    void doSQLCommand(String command, Object[] params);

    void dumpTable(String tableName, String command);

    void bind(List<String> vars, String command);

    int execute(PreparedStatement ps, Object[] params, FetchInterceptor interceptor) throws SQLException;

    void doSQLCommand(String command, Object[] params, FetchInterceptor interceptor);

    void listTables(String name);

    void listViews(String name);

    void listSequences(String name);

    void listSynonyms(String name);

    void listProcedures(String name);

    void listObjectByType(ObjectTypes type, String name);

    void source(String sourceSql, String objName);

    void setAutoCommit(String pOnOff);

    String getAutoCommit();

    String getCurrentUser();

    boolean isDoBindings();

    void setDoBindings(boolean doBindings);

    String getUri();

    List<String> getAutocompleteObjects();
    List<String> getColumns(String tableName);

    String valueToString(int dataType, Object value);

    DateFormat getDateFormat();

    DateFormat getDatetimeFormat();

    String repr(Object val, int sqlType);

    void commit();

    void rollback();
}
