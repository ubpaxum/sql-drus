package org.ibro.sqldrus.dialect;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/25/16
 * Time: 5:11 PM
 * Short description of purpose.
 */
public interface DB2SqlConstants {
      String DIALECT_NAME = "DB2";
      String JDBC_DRIVER_NAME = "com.ibm.db2.jcc.DB2Driver" ;

      String SQL_USER_TABLES = "select object_name as name, object_type as type  " +
            "from all_objects " +
            "where owner=? <<object_type_clause>> and object_name like ? " +
            "order by 1,2";

      String SQL_PROC_SOURCE = "select text||chr(10)||'/' as txt " +
            "from  all_source where name = ? and owner =? order by type";

      String SQL_TAB_COLUMNS = "select column_name, data_type, nvl(data_precision, data_length), data_scale, nullable, data_default " +
            "from all_tab_columns " +
            "where owner=? and table_name=? order by column_id";

      String SQL_COLUMNS = "select column_name " +
            "from all_tab_columns " +
            "where owner=? and table_name=? order by column_id";

      String SQL_PK_CONSTRAINTS = "select 'index '||c.index_name||" +
            "decode(c.constraint_type,'P',' primary key (','U',' unique key (') ||\n" +
            "(select listagg(column_name,',') within group (order by column_position) as pk from all_ind_columns where index_owner=c.owner and index_name=c.index_name) ||\n" +
            "') ' as expr \n" +
            "from all_constraints c where c.table_name=? and c.owner=? and c.constraint_type in ('P','U')";

      String SQL_CHECK_CONSTAINTS = "select sc.constrname::varchar(25) as constraint, ssc.checktext as expression " +
            "from informix.sysconstraints sc, informix.systables st, informix.syschecks ssc " +
            "where st.tabname=? and st.owner=? and sc.tabid=st.tabid and sc.constrid=ssc.constrid and ssc.type='T' order by ssc.constrid, ssc.seqno;";

      String SQL_REF_CONSTRAINTS = "select 'constraint \"'||c.constraint_name||'\" foreign key ('||" +
            "   (select listagg(column_name,',') within group (order by position)" +
            "   from ALL_CONS_COLUMNS x where x.owner=c.owner and x.table_name=c.table_name and x.constraint_name=c.constraint_name) ||" +
            "   ') references '||c1.owner||'.'||c1.table_name || ' (' ||" +
            "   (select listagg(column_name,',') within group (order by position) " +
            "   from ALL_CONS_COLUMNS x where x.owner=c1.owner and x.table_name=c1.table_name and x.constraint_name=c1.constraint_name) ||')' as expr\n" +
            "from all_constraints c, all_constraints c1\n" +
            "where c.table_name=? and c.owner=? and c.constraint_type='R'" +
            "  and c1.owner=c.r_owner and c1.constraint_name=c.r_constraint_name";

      String SQL_REF_BY_CONSTRAINTS = "select 'table '||c.owner||'.'||c.table_name ||" +
            "   ' constraint \"'||c.constraint_name ||" +
            "   '\" foreign key('||" +
            "   (select listagg(column_name,',') within group (order by position) " +
            "   from ALL_CONS_COLUMNS x where x.owner=c.owner and x.table_name=c.table_name and x.constraint_name=c.constraint_name) ||" +
            "   ') REFERENCES '||c2.owner||'.'||c2.table_name || '(' || " +
            "   (select listagg(column_name,',') within group (order by position) " +
            "   from ALL_CONS_COLUMNS x where x.owner=c1.owner and x.table_name=c1.table_name and x.constraint_name=c1.constraint_name)||')' as expr " +
            "from all_constraints c2, all_constraints c, all_constraints c1 " +
            "where c2.table_name=?  and c2.owner=? and c2.constraint_type in ('P','U') and" +
            "  c.r_owner=c2.owner and c.r_constraint_name=c2.constraint_name and c.constraint_type='R' and" +
            "  c1.owner=c.r_owner and c1.constraint_name=c.r_constraint_name";

      String SQL_INDEX_DESCRIBE = "select index_name, owner, index_type, UNIQUENESS, num_rows " +
            "from all_indexes where table_name=? and table_owner=?";

      String SQL_COLUMN = "select column_name from all_ind_columns where index_owner=? and index_name=? order by column_position";

      String SQL_TABLES = "select object_type, object_name, owner,  status, created from all_objects where object_name like ? and owner=? order by object_name, object_type";

      String SQL_TRIGGER = "select TRIGGER_BODY||chr(10)||'/' as txt " +
            "from  all_triggers where table_name = ? and owner =?";

      String SQL_AUTOCOMPLETE_OBJECTS =  "select lower(object_name) from user_objects where object_type in ('TABLE','VIEW') order by object_name";
}
