package org.ibro.sqldrus.dialect;

import org.ibro.sqldrus.Verbose;
import org.ibro.sqldrus.command.SQLCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.ibro.sqldrus.dialect.InformixSqlConstants.*;
import static org.ibro.sqldrus.util.StringUtils.padString;
import static org.ibro.sqldrus.util.StringUtils.rtrim;

public class InformixDialect extends AbstractSqlDialect {
    private static final Logger log = LoggerFactory.getLogger(InformixDialect.class);

    public InformixDialect(SQLCommands sqlCmd){
        super(sqlCmd,DIALECT_NAME,JDBC_DRIVER_NAME);
        setDateFormat(new SimpleDateFormat("MM-dd-yyyy"));
        setDatetimeFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    private String getTypeName( int type, int length){
        int realType = type & 0x00FF;

        String isNull = ( (type & 0x0100) > 0) ? "NOT NULL":"NULL";
        String typeName = null;
        switch (realType){
            case 0  : typeName = "CHAR("+length+")";
                      break;
            case 1  : typeName = "SMALLINT";
                      break;
            case 2  : typeName = "INTEGER";
                      break;
            case 3  : typeName = "FLOAT";
                      break;
            case 4  : typeName = "SMALLFLOAT";
                      break;
            case 5  : typeName = "DECIMAL("+length/256+","+length%256+")";
                      break;
            case 6  : typeName = "SERIAL";
                      break;
            case 7  : typeName = "DATE";
                      break;
            case 8  : typeName = "MONEY("+length+")";
                      break;
            case 9  : typeName = "NULL";
                      break;
            case 10  : typeName = "DATETIME";
                      break;
            case 11  : typeName = "BYTE("+length+")";
                      break;
            case 12  : typeName = "TEXT("+length+")";
                      break;
            case 13 : typeName = "VARCHAR("+length+")";
                      break;
            case 14 : typeName = "INTERVAL";
                      break;
            case 15 : typeName = "NCHAR("+length+")";
                      break;
            case 16 : typeName = "NVARCHAR("+length+")";
                      break;
            case 17 : typeName = "INT8";
                      break;
            case 18 : typeName = "SERIAL8";
                      break;
            case 19 : typeName = "SET";
                      break;
            case 20 : typeName = "MULTISET";
                      break;
            case 21 : typeName = "LIST";
                      break;
            case 22 : typeName = "UNNAMED ROW";
                      break;
            case 23 : typeName = "COLLECTION";
                break;
            case 40 : typeName = "LVARCHAR("+length+")";
                      break;
            case 41 : typeName = "CLOB,BLOB,BOOLEAN";
                      break;
            case 43 : typeName = "LVARCHAR(client-side)";
                break;
            case 45 : typeName = "BOOLEAN";
                break;
            case 52 : typeName = "BIGINT";
                break;
            case 53 : typeName = "BIGSERIAL";
                break;
            case 2061: typeName = "IDSSECURITYLABEL";
                break;
            case 4118: typeName = "Named ROW";
                      break;
            default: typeName="--unknown--";
        }
        return padString(typeName, 20, ' ', true)+padString(isNull, 8, ' ', false);
    }

    private void describeTableOrView( int tabId){
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = conn.prepareStatement(SQL_TAB_COLUMNS);
            ps.setInt(1,tabId);
            rs = ps.executeQuery();
            sqlCmd.output("COLUMN NAME                   TYPE                NULL      DEFAULT",Verbose.HEADINGS);
            sqlCmd.output("-------------------------------------------------------------------",Verbose.HEADINGS);
            String def_value = null;
            int idx = 0;
            while ( rs.next() ){
                def_value = rs.getString(4);
                if (def_value == null) def_value = "";
                else {
                    def_value = def_value.trim();
                    if ( (idx=def_value.indexOf(' '))>0){
                        def_value = def_value.substring(idx+1);
                    }
                }
                sqlCmd.output(padString(rs.getString(1), 30, ' ', true) + getTypeName(rs.getInt(2), rs.getInt(3)) + rtrim(" "+ def_value),Verbose.ROWS );
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally{
            try{
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }
    }

    @Override
    public void describe(String objName){
        String owner = getObjectOwner(objName);
        String objectName = getObjectName(objName);
        String likeName = toLikePattern(objectName);
        String line = "";
        PreparedStatement ps = null;
        ResultSet rs = null;


        try {
            ps = conn.prepareStatement(SQL_TABLES);
            ps.setString(1, likeName);
            ps.setString(2, owner);
            rs = ps.executeQuery();
            while ( rs.next() ){
                int tabId = rs.getInt(1);
                String tabType = rs.getString(2);
                String objectType = ObjectTypes.fromCode(tabType).name();

                line = objectType+": "+rs.getString(4).trim()+"."+rs.getString(3) /*+" id: "+tabId+" Created: "+rs.getString(5)*/;
                if (tabType.equals("T") || tabType.equals("V")) {
                    line += " columns: " + rs.getInt(7) + " rowsize: " + rs.getInt(6)  + " # rows: " + Math.round(rs.getFloat(8));
                    sqlCmd.output(line, Verbose.HEADINGS);
//                    sqlCmd.output(padString("", line.length(), sqlCmd.PAD_CHARACTER, true), Verbose.HEADINGS);
                    describeTableOrView(tabId);
                } else {
                    sqlCmd.output(line, Verbose.HEADINGS);
                }
                sqlCmd.output("", Verbose.FOOTERS);
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally{
            try {
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }

    }

    @Override
    public void describeIndexes(String tabName){
        String owner = getObjectOwner(tabName);
        String objectName = getObjectName(tabName);
        String line = "";
        PreparedStatement ps = null;
        PreparedStatement psCols = null;
        ResultSet rs = null;
        ResultSet rsCols;

        try {
            ps = conn.prepareStatement(SQL_INDEX_DESCRIBE);
            ps.setString(1, objectName.toLowerCase() );
            ps.setString(2, owner.toLowerCase() );

            psCols = conn.prepareStatement(SQL_COLUMN);
            line = "INDEXES of "+objectName;
            sqlCmd.output(line, Verbose.ALL);
            sqlCmd.output("INDEX NAME                    TYPE                 LEVELS  LEAVES COLUMNS", Verbose.HEADINGS);
            sqlCmd.output("----------------------------------------------------------------------------------------", Verbose.HEADINGS);
            rs = ps.executeQuery();
            while ( rs.next() ){
                int    tabId    = rs.getInt(1);
                String idxName  = rs.getString(2).trim();
                String idxType  = rs.getString(4);
                int    levels   = rs.getInt(5);
                int    leaves   = rs.getInt(6);

                line = padString(idxName,30,' ',true)+padString(idxType,20,' ',true)+padString(""+levels,8,' ',false)+padString(""+leaves,8,' ',false);

                int currPart = 7;
                int colNo = 0;

                while (  (colNo=rs.getInt(currPart)) != 0   ){
                    log.debug("colNo: "+colNo);
                    psCols.setInt(1,tabId);
                    psCols.setInt(2,colNo);
                    rsCols = psCols.executeQuery();
                    if ( rsCols.next() )
                        line += " "+rsCols.getString(1).trim();
                    currPart ++;
                    rsCols.close();
                }
                sqlCmd.output(line, Verbose.ROWS);
            }
        } catch (SQLException e){
            sqlCmd.error(e);
        } finally {
            try {
                if (psCols != null ) psCols.close();
                if (rs != null ) rs.close();
                if (ps != null ) ps.close();
            } catch (SQLException e ){}
        }

    }

    @Override
    public void sourceProcedure(String procName){
        String objName= getObjectName(procName).toUpperCase();
        String objOwner = getObjectOwner(procName);
        source(SQL_PROC_SOURCE, objOwner + "." + objName);
    }

    @Override
    public void sourceTrigger(String trigName){
        String objName= getObjectName(trigName).toUpperCase();
        String objOwner = getObjectOwner(trigName);
        source(SQL_TRIGGER, objOwner + "." + objName);
    }

    public void listObjectByType(ObjectTypes type, String name){
        String objName= getObjectName(name);
        String objOwner = getObjectOwner(name);
        String likeName = toLikePattern(objName);
        doSQLCommand(SQL_USER_TABLES, new Object[]{objOwner, type.code(), likeName, objOwner, type.code(), likeName});
    }

    public void describeConstraints(String tabName){
        String objName = getObjectName(tabName);
        String objOwner = getObjectOwner(tabName);
        Verbose oldVerbose = sqlCmd.getVerboseOutput();
        sqlCmd.setVerboseOutput(Verbose.HEADINGS);
        sqlCmd.output("\nPrimary and unique constraints", Verbose.HEADINGS);
        doSQLCommand(SQL_PK_CONSTRAINTS, new Object[]{objName, objOwner});
        sqlCmd.output("\nCheck constraints", Verbose.HEADINGS);
        doSQLCommand(SQL_CHECK_CONSTAINTS, new Object[]{objName, objOwner});
        sqlCmd.output("\nReferences", Verbose.HEADINGS);
        doSQLCommand(SQL_REF_CONSTAINTS, new Object[]{objName, objOwner});
        sqlCmd.output("\nReferenced by", Verbose.HEADINGS);
        doSQLCommand(SQL_REF_BY_CONSTAINTS, new Object[]{objName, objOwner});
        sqlCmd.setVerboseOutput(oldVerbose);
    }


    private String toLikePattern(String name){
        if ( name == null ) return "%";
        if ( name.contains("*") ) return name.replace('*','%');
        return name;
    }

    @Override
    public String getObjectName(String objName) {
        String name=super.getObjectName(objName);
        return name==null?null:name.toLowerCase();
    }

    @Override
    public String getObjectOwner(String objName) {
        return  super.getObjectOwner(objName).toLowerCase();
    }

    @Override
    public String getCurrentUser() {
        return super.getCurrentUser().toLowerCase();
    }

    @Override
    public List<String> getAutocompleteObjects() {
        return resultAsList( SQL_AUTOCOMPLETE_OBJECTS );
    }

    @Override
    public List<String> getColumns(String tableName) {
        String owner = getObjectOwner(tableName);
        String objectName = getObjectName(tableName);
        return getColumns(owner,objectName, SQL_COLUMNS );
    }

}
