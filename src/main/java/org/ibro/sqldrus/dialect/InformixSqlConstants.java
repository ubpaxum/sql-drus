package org.ibro.sqldrus.dialect;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/25/16
 * Time: 5:11 PM
 * Short description of purpose.
 */
public interface InformixSqlConstants {
      String DIALECT_NAME = "Informix";
      String JDBC_DRIVER_NAME = "com.informix.jdbc.IfxDriver" ;

      String SQL_USER_TABLES = "select tabname::varchar(35) as name, " +
            "decode(tabtype,'T','table','V','view','Q', 'sequence','S','synonym','unknown')::varchar(15) as type  " +
            "from informix.systables " +
            "where owner=? and tabtype like ? and tabname like ? " +
            "union all "+
            "select procname::varchar(35) as name, decode(isproc,'t','procedure','function')::varchar(15) as type from informix.sysprocedures " +
            "where owner=? and 'X' like ? and procname like ? "+
            "order by 1,2";

      String SQL_PROC_SOURCE = "select data as text " +
            "  FROM informix.sysprocbody a, informix.sysprocedures b " +
            " WHERE a.procid = b.procid  and b.procname=? and b.owner=? and datakey = 'T' ORDER BY seqno";


      String SQL_TAB_COLUMNS = "SELECT a.colname, a.coltype, a.collength, "+
            "decode(b.type,'C','CURRENT', 'U', 'USER', 'S', 'DBSERVERNAME', 'T', 'TODAY', 'N','','L',b.default,'') as default  "+
            "FROM informix.syscolumns a left join informix.sysdefaults b on a.tabid=b.tabid and a.colno=b.colno "+
            "WHERE a.tabid=?  "+
            "ORDER by a.colno";

      String SQL_COLUMNS = "SELECT a.colname "+
            "FROM informix.syscolumns a left join informix.systables b on a.tabid=b.tabid "+
            "WHERE b.owner=? and b.tabname=?"+
            "ORDER by a.colno";

      String SQL_PK_CONSTRAINTS = "select pc.constrname::varchar(30) as pk_constr_name, pc.idxname::varchar(30) as idx_name, decode(pc.constrtype,'P','primary','unique') as type, " +
            "(select col.colname from sysindexes si, syscolumns col where si.idxname=pc.idxname and si.tabid=pt.tabid and col.tabid=si.tabid and col.colno=si.part1)::varchar(25) as pk_column "+
            "from informix.systables pt, informix.sysconstraints pc where pc.tabid=pt.tabid and pt.tabname=? and pt.owner=? and pc.constrtype in ('P','U');";


      String SQL_CHECK_CONSTAINTS = "select sc.constrname::varchar(25) as constraint, ssc.checktext as expression " +
            "from informix.sysconstraints sc, informix.systables st, informix.syschecks ssc "+
            "where st.tabname=? and st.owner=? and sc.tabid=st.tabid and sc.constrid=ssc.constrid and ssc.type='T' order by ssc.constrid, ssc.seqno;";

      String SQL_REF_CONSTAINTS = " select rt.tabname::varchar(25) as ref_table, pc.constrname::varchar(30) as fk_constr_name,  rc.constrname::varchar(30) as ref_constr_name, "+
            "(select col.colname from sysindexes si, syscolumns col where si.idxname=pc.idxname and si.tabid=pt.tabid and col.tabid=si.tabid and col.colno=si.part1)::varchar(25) as pk_column, "+
            "(select col.colname from sysindexes si, syscolumns col where si.idxname=rc.idxname and si.tabid=rt.tabid and col.tabid=si.tabid and col.colno=si.part1)::varchar(25) as fk_column "+
            "from informix.sysreferences sr, informix.systables pt, informix.sysconstraints pc, informix.systables rt, informix.sysconstraints rc where sr.constrid=pc.constrid and pc.tabid=pt.tabid and pt.tabname=? and pt.owner=? and sr.ptabid=rt.tabid and sr.primary=rc.constrid;";

      String SQL_REF_BY_CONSTAINTS = "select pt.tabname::varchar(25) as ref_table, pc.constrname::varchar(30) as fk_constr_name,  rc.constrname::varchar(30) as pk_constr_name, "+
            "(select col.colname from sysindexes si, syscolumns col where si.idxname=pc.idxname and si.tabid=pt.tabid and col.tabid=si.tabid and col.colno=si.part1)::varchar(25) as fk_column, "+
            "(select col.colname from sysindexes si, syscolumns col where si.idxname=rc.idxname and si.tabid=rt.tabid and col.tabid=si.tabid and col.colno=si.part1)::varchar(25) as pk_column "+
            "from informix.sysreferences sr, informix.systables pt, informix.sysconstraints pc, informix.systables rt, informix.sysconstraints rc where sr.constrid=pc.constrid and pc.tabid=pt.tabid and rt.tabname=? and rt.owner=? and sr.ptabid=rt.tabid and sr.primary=rc.constrid;";

      String SQL_INDEX_DESCRIBE = "select six.tabid, six.idxname, six.owner, decode(six.idxtype,'U','UNIQUE','u','UNIQUE BITMAP','D','NON-UNIQUE','G','GENERALIZED','g','GENERILIZED BITMAP', 'd','NON-UNIQUE BITMAP') as index_type, levels, leaves,"+
            "part1,part2,part3,part4,part5,part6,part7,part8,part9,part10,part11,part12,part13,part14,part15,part16 "+
            "from sysindexes six, systables ste where six.tabid=ste.tabid and ste.tabname=? and ste.owner=?";

      String SQL_COLUMN = "select colname from syscolumns where tabid=? and colno=?";

      String SQL_TABLES = "select tabid, tabtype, tabname, owner,  created, rowsize, ncols, nrows from informix.systables where tabname like ? and owner=? order by tabname, tabtype";

      String SQL_TRIGGER = "select b.data as trigger from informix.systables t, informix.systriggers g,  informix.systrigbody b " +
            "where t.tabname=? and t.owner=? and t.tabid=g.tabid and g.trigid=b.trigid  and b.datakey in ('A','D') " +
            "order by g.trigid, b.datakey desc, b.seqno";

      String SQL_AUTOCOMPLETE_OBJECTS =  "select tabname from informix.systables where tabtype in ('T','V') and owner=current_user order by tabname";
}
