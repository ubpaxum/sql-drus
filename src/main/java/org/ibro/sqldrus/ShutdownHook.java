package org.ibro.sqldrus;


public class ShutdownHook extends Thread {
    CommandParser cm = null;
    public ShutdownHook( CommandParser cm ){
        this.cm = cm;
    }

    public void run() {
          if (cm!=null) cm.process("QUIT");
    }

}