package org.ibro.sqldrus;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/15/16
 * Time: 2:01 PM
 * Short description of purpose.
 */
public enum Verbose {
    SILENT(0),
    ERROR(5),
    ROWS(10),
    HEADINGS(20),
    FOOTERS(30),
    ALL(40);

    private final int level;
    Verbose(int level){
        this.level = level;
    }

    public int level(){
        return level;
    }
}
