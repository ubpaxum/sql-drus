package org.ibro.sqldrus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 11/10/16
 * Time: 11:09 AM
 * Short description of purpose.
 */
public interface SqlConstants {

    public static final int MAX_COL_SIZE  = 1000;
    public static final int BULK_SIZE  = 1000;

    // for command parser
    public final static List<String> SUPPORTED_DATA_TYPES = Arrays.asList("INTEGER", "NUMERIC", "VARCHAR", "DATE", "SMALLINT");
    public final static char   COMMAND_SEPARATOR   = ';';
    public final static String LAST_COMMAND        = "/";
    public final static String EXEC_FILE_COMMAND   = "@";
    public final static String EXEC_HOST_COMMAND   = "!";
    public final static String COMMENT             = "--";
    public final static String BUFFER_FILE         = ".sql-drus.buf";
    public final static String WINDOWS_DEFAULT_EDITOR    = "notepad.exe";
    public final static String WINDOWS_EXEC_PROCESSOR      = "cmd.exe";
    public final static String WINDOWS_EXEC_PROCESSOR_ARGS = "/c";
    public final static String UNIX_DEFAULT_EDITOR       = "xterm -e vi";
    public final static String UNIX_EXEC_PROCESSOR      = "/bin/sh";
    public final static String UNIX_EXEC_PROCESSOR_ARGS = "-c";

    public final static String USAGE =
            "usage: sql-drus [-h] [-q] [-n] [-f sqlFile]\n"+
             "\t-v|--version - print version informaiton and exit\n"+
             "\t-h|--version - this screen\n"+
             "\t-q|--quiet - be more quiet (don't print version message)\n" +
             "\t-n|--non-interactive - do not enter in read loop - just execute  commands from file(s) and/or standard input\n" +
             "\t-f sqlFile - SQL file to execute. You can put many files with multiple -f options.\n";

    public static final String [][] HELP_STRING  ={
            {"^CONN|^CONNECT","CONN[ECT] {connectionAlias | full_jdbc_url} [user [password]] [--keep]" },
            {"^DISCONN|^DISCONNECT","DISCONN[ECT] [connectionName]" },
            {"^SHOWCONN","SHOWCONN - show active JDBC connections (current is marked with *)" },
            {"^SWITCH","SWITCH conneciotonName - switches current connection" },
            {"^QUIT|^EXIT","QUIT | EXIT - exits sql-drus" },
            {"^DESC|^DESCRIBE","DESC[RIBE] [owner.]{pattern} ..." },
            {"^INDE|^INDEXES","INDE[XES] [owner.]{table} ... - show indexes of table" },
            {"^CONSTR|^CONSTRAINTS","CONSTR[AINTS] [owner.]{table} ... - show constraints of table" },
            {"RELOAD","RELOAD - reload connection definitions from connection.property file" },
            {"^LS","LS [owner.][pattern] ... - list user tables, views, sequences, synonyms and stored procedures" },
            {"^LT","LT [owner.][pattern] ...- list user tables" },
            {"^LV", "LV [owner.][pattern]... - list user views" },
            {"^LQ","LQ [owner.][pattern] ...- list user sequences" },
            {"^LP", "LP [owner.][pattern] ...- list user synonyms" },
            {"^LX", "LX [owner.][pattern] ...- list user procedures" },
            {"^SRC", "SRC [owner.]{proc_name} ... - dump source of stored procedure" },
            {"^TRIG|^TRIGGER", "TRIG[GER] [owner.]{table} ... - dump triggers for this table (if any)" },
            {"^DUMP", "DUMP tableName select ... - dump this as select as insert into tableName" },
            {"^COPY", "COPY tableName into {connectionName} [WHERE {where_clause}] - copy data from tableName to another connection/database using bulk insert" },
            {"^BIND", "BIND colName1[ colName2...colNameN] select ... - bind value for each colNameX for each row in result set" },
            {"^SPOOL","SPOOL {OFF | CONSOLE | file}" },
            {"^SETENV",
                    "SETENV [VERBOSE  {ALL | ROWS | HEADINGS | FOOTERS | SILENT | ERROR }] |\n"+
                    "       [AUTOCOMMIT {ON|OFF}] |\n"+
                    "       [ONERROR {CONTINUE|EXIT|ROLLBACK|COMMIT}] |\n"+
                    "       [EDITOR {program_name}] |\n"+
                    "       [MAX_COL_SIZE {max_column_size}]\n"+
                    "       [BULK_SIZE {row_count}]\n"+
                    "       [TIMING {ON|OFF}]" },
            {"^SETVAR","SETVAR varName {INTEGER|NUMERIC|VARCHAR|DATE} value\n\tWithout arguments to print current bind variables."+
                    " No enclosure quotes or apostrophes are needed for value.\n\t"+
                    "Bind them in SQL commands as :varName" },
            {"^/","/ - execute last executed dml or ddl (not including sql-drus commands )" },
            {"^SQL","SQL [intercept {connectionName|this} [bulk] SQL...]; - execute SQL statement. If intercept clause is present the result from first SQL command can be binded in the second SQL command - e.g. select * from cat intercept test_ifx insert into test (c1,c2) values (:1,:2);"},
            {"^@\\w+","@file - read commands from file and execute them. Do not stop on error." },
            {"^!\\w+","!command or \\!command - executes host command and get the output of it to console. you may need to escape '!' character as it has special meaning in *nix terminals." },
            {"^ED|EDIT","ed [file] - edit file with external text editor" },
            {"^CD","cd [dir] - change directory to dir. If dir is ommited, show current working directory" },
            {"^HISTORY","HISTORY - prints the command line history"},
            {"^VERSION","VERSION - print version information"},
            {"^HELP","HELP - this screen"}
    } ;

    public static final String BLACK = "\u001B[30m";
    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";
    public static final String BLUE = "\u001B[34m";
    public static final String PURPLE = "\u001B[35m";
    public static final String CYAN = "\u001B[36m";
    public static final String WHITE = "\u001B[37m";
    //Reset code
    public static final String RESET = "\u001B[0m";


}
