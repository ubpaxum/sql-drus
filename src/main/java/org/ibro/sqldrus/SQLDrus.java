package org.ibro.sqldrus;

import org.ibro.sqldrus.completer.SqlDrusCompleter;
import org.ibro.sqldrus.completer.SqlDrusSyntaxCompleter;
import org.ibro.sqldrus.config.SqlDrusConfig;
import org.ibro.sqldrus.config.Version;
import org.jline.reader.Completer;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.terminal.impl.DumbTerminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static org.ibro.sqldrus.SqlConstants.*;


public class SQLDrus {
    private String promptColor = GREEN;
    private String textColor   = WHITE;

    private final static String COMMAND_LINE_PROMPT = "sql-drus";
    private final static String COMMAND_LINE_CONTINUE = "> ";
    private String promptString = null;
    private String continueString = null;
    private SqlDrusConfig config = null;
    // main input loop
    private Terminal terminal = null;
    private InputStream in ;
    private PrintStream out ;
    private Completer completer = null;
    private CommandParser cp = null;
    private Verbose verbose =  Verbose.ALL;
    private List<String> execFiles ;
    private List<String> keywords ;
    private boolean nonInteractive = false;
    // last command processor result
    // -1 -> exit sql-drus
    // 0 - executed, go to next command
    // >0 - continue reading next line (multiline command)
    private int cpRes = 0;

    public SQLDrus() throws IOException {
        init();
        this.terminal = TerminalBuilder.terminal();
        this.out = new PrintStream( System.out );
        this.in  = terminal.input();
    }

    public SQLDrus(InputStream in, PrintStream out) throws IOException {
        init();
        this.in = in;
        this.out = out;
//        this.terminal = TerminalBuilder.builder().streams(in,out).build(); //  terminal();
        this.terminal = new DumbTerminal(in, out);
    }

    private void init(){
        config = new SqlDrusConfig();
        this.execFiles = new ArrayList<>();
        this.promptString = promptColor+COMMAND_LINE_PROMPT+COMMAND_LINE_CONTINUE+textColor;
        this.continueString = promptColor+COMMAND_LINE_CONTINUE+textColor;
        setPrompt(COMMAND_LINE_PROMPT);
        SqlDrusCompleter keywordCompleter = new SqlDrusCompleter(config);
        this.keywords = keywordCompleter.getKeywords();
    }

    public void setPrompt(String newPrompt){
        if ( newPrompt == null || "".equals(newPrompt) )  resetPrompt();
        else this.promptString =promptColor+newPrompt+COMMAND_LINE_CONTINUE+textColor;
    }

    public void addExecFile(String fileName){
        execFiles.add(fileName);
    }

    public void resetPrompt(){
        this.promptString = promptColor+COMMAND_LINE_PROMPT+COMMAND_LINE_CONTINUE+textColor;
    }

    public String getPrompt(){
        return this.promptString;
    }

    public String getContinuePrompt(){
        return continueString;
    }

    public SqlDrusConfig getConfig() {
        return config;
    }

    public Verbose getVerbose() {
        return verbose;
    }

    public void setVerbose(Verbose verbose) {
        this.verbose = verbose;
    }

    public void usage(){
        out.println(USAGE);
    }

    public void version(){
        out.println(Version.VERSION_MESSAGE);
    }

    public boolean isNonInteractive() {
        return nonInteractive;
    }

    public void setNonInteractive(boolean nonInteractive) {
        this.nonInteractive = nonInteractive;
        setVerbose(Verbose.FOOTERS);
    }

    public void start() throws IOException {
        if ( terminal instanceof DumbTerminal )
            setNonInteractive(true);

        this.cp = new CommandParser(out, this,verbose);
        ShutdownHook shutdownHook = new ShutdownHook(cp);
        Runtime.getRuntime().addShutdownHook(shutdownHook);

        for (String file:  getConfig().getStartupFiles() ){
            if( cpRes>=0) cpRes = cp.execFileCommand( file );
        }

        // execute files if any
        for (String file:  execFiles ){
            if( cpRes>=0) cpRes = cp.execFileCommand( file );
        }

        String line ;
        if ( terminal instanceof DumbTerminal ) { // piping - get the input stream and process everything there
            BufferedReader reader = new BufferedReader(new InputStreamReader(terminal.input()));
            while (cpRes >= 0 && (line = reader.readLine()) != null) {
                cpRes = cp.process(line);
            }
        }
    }

    public void readLoop() throws IOException {
        if ( terminal == null || terminal instanceof DumbTerminal ) return;
        if ( isNonInteractive() )  return;

        this.completer = new SqlDrusSyntaxCompleter(config);
        String line;
        LineReader lineReader = LineReaderBuilder.builder()
//                        .completer(new SqlDrusCompleter(sqlDrus.getConfig()))
                .completer(completer)
//                        .history(new DefaultHistory())
//                        .highlighter(new DefaultHighlighter())
                .terminal(terminal)
                .build();
        lineReader.setVariable(LineReader.HISTORY_FILE, new File(config.getHistoryFile()) );
        try {
            cp.start();
            while (cpRes >= 0 && (line = lineReader.readLine((cpRes > 0) ? getContinuePrompt() : getPrompt())) != null) {
                cpRes = cp.process(line);
            }
        } catch (EndOfFileException e) {}
        finally {
            cp.stop();
        }

    }

    public Completer getCompleter(){
        return completer;
    }

    public void stop(){
        if ( terminal != null ) terminal.flush();
        if ( out != null )  out.close();
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public static void main(String[] args) {
        int resultCode = 0;
        // disbale jline logger
        disableLogger();
        SQLDrus sqlDrus = null;
        try{
            sqlDrus = new SQLDrus();
            parseArgs(sqlDrus,args);
            Logger log =  LoggerFactory.getLogger(SQLDrus.class);
            sqlDrus.start();
            sqlDrus.readLoop();
        } catch (IOException e){
            e.printStackTrace();
            resultCode = -1;
        } finally {
            if (sqlDrus!=null) sqlDrus.stop();
        }
        System.exit(resultCode);
      }

    private static void parseArgs(SQLDrus sqlDrus, String[] args) {
        for ( int idx=0; idx < args.length; idx ++ ){
            String arg = args[idx];
            switch (arg){
                case "-q":
                case "--quiet":
                    sqlDrus.setVerbose(Verbose.FOOTERS);
                    break;
                case "-f":
                    sqlDrus.setNonInteractive(true);
                    if ( idx+1 <args.length ){
                        sqlDrus.addExecFile( args[idx+1] );
                        idx ++;
                    } else {
                        throw new RuntimeException("Filename missing after -f argument");
                    }
                    break;
                case "-h":
                case "--help":
                    sqlDrus.version();
                    sqlDrus.usage();
                    System.exit(0);
                    break;
                case "-v":
                case "--version":
                    sqlDrus.version();
                    System.exit(0);
                    break;
                case "-n":
                case "--non-interactive":
                    sqlDrus.setNonInteractive(true);
                    break;
                default:
                    throw new RuntimeException("Unknown argument "+arg);
            }
        }
    }

    private static void disableLogger(){
        java.util.logging.Logger jlineLogger = java.util.logging.Logger.getLogger("org.jline");
        if (jlineLogger != null) jlineLogger.setLevel(Level.OFF);
    }

}