package org.ibro.sqldrus;

import org.ibro.sqldrus.command.BasicSQLCommands;
import org.ibro.sqldrus.command.SQLCommands;
import org.ibro.sqldrus.completer.SqlDrusSyntaxCompleter;
import org.ibro.sqldrus.config.Version;
import org.ibro.sqldrus.dialect.ObjectTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import static org.ibro.sqldrus.SqlConstants.*;

public class CommandParser {
    private static final Logger log = LoggerFactory.getLogger(CommandParser.class);

    private SQLCommands sqlCmd = null;
    private SQLDrus sqlDrus = null;
    private boolean timing = false;
    private String shell = null;
    private String shell_args = null;
    private String editor = null;
    private String command = null;
    private String lastCommand = null;
    private String firstCommand = null;
    private boolean isCreateRoutine = false;
    private String cwd = System.getProperty( "user.dir" );
    private PrintStream out ;
    private Verbose verbose;

    public CommandParser (PrintStream pwOut, SQLDrus sqlDrus) {
        this(pwOut,sqlDrus,Verbose.ALL);
    }

    public CommandParser (PrintStream pwOut, SQLDrus sqlDrus, Verbose verbose){
        out = pwOut;
        this.sqlDrus = sqlDrus;
        sqlCmd = new BasicSQLCommands(pwOut, sqlDrus.getConfig(), verbose );
        sqlCmd.output(Version.VERSION_MESSAGE,Verbose.ALL);
        String osSystem = System.getProperty("os.name");
        if (osSystem.toLowerCase().indexOf("windows") != -1 ){
            shell = WINDOWS_EXEC_PROCESSOR;
            shell_args = WINDOWS_EXEC_PROCESSOR_ARGS;
            editor = WINDOWS_DEFAULT_EDITOR;
        } else {
            shell = UNIX_EXEC_PROCESSOR;
            shell_args = UNIX_EXEC_PROCESSOR_ARGS;
            editor = UNIX_DEFAULT_EDITOR;
        }
    }

    public void setTiming( String onOff){
        if ( onOff.equalsIgnoreCase("ON") ) timing = true;
        else if ( onOff.equalsIgnoreCase("OFF") ) timing = false;
        else sqlCmd.output("Valid values for timing are ON and OFF",Verbose.ALL);
    }

    public String getTiming(){
        return timing ? "ON" : "OFF";
    }

    public void setEditor( String edit_prog){
        editor = edit_prog;
    }

    public String getEditor(){
        return editor;
    }

    public boolean execute(String command){
        boolean result = true;
        long startTime = 0;
        log.info("execute: " + command);
        StringTokenizer st = new StringTokenizer(command);
        if (timing) startTime = System.currentTimeMillis();
        try {
            if (st.hasMoreTokens()){
                String cmd = st.nextToken();
                boolean hasParameters = st.hasMoreTokens();
                if ( "CONNECT".equalsIgnoreCase(cmd) || "CONN".equalsIgnoreCase(cmd) ){
                    if ( hasParameters ) {
                        String connString = st.nextToken();
                        String userName = null;
                        String password = null;
                        boolean keepCurrent = false;
                        while ( st.hasMoreTokens() ){
                            String val = st.nextToken();
                            if (  val.equalsIgnoreCase("--keep")  ) keepCurrent = true;
                            else if ( userName == null ) userName = val;
                            else password = val;
                        }
                        sqlCmd.connect(connString, userName, password, keepCurrent);
                        if ( sqlCmd.hasConnection() && sqlDrus.getCompleter()!=null )
                            ((SqlDrusSyntaxCompleter) sqlDrus.getCompleter()).setAutocompleteObjects( sqlCmd.getAutocompleteObjects() );
                        sqlDrus.setPrompt(sqlCmd.getConnectionName());
                    } else {
                        printCommandHelp(cmd);
                    }
                } else if ("DISCONNECT".equalsIgnoreCase(cmd) || "DISCONN".equalsIgnoreCase(cmd) ){
                    if ( st.hasMoreTokens() ){
                        while (st.hasMoreTokens()){
                            sqlCmd.disconnect( st.nextToken() );
                        }
                    } else sqlCmd.disconnect( sqlCmd.getConnectionName() );
                    sqlDrus.setPrompt(sqlCmd.getConnectionName());
                } else if ("SHOWCONN".equalsIgnoreCase(cmd) ){
                    sqlCmd.showConnections();
                } else if ("SWITCH".equalsIgnoreCase(cmd) ){
                    if ( st.hasMoreTokens() ){
                        sqlCmd.switchConnection(st.nextToken());
                        sqlDrus.setPrompt(sqlCmd.getConnectionName());
                        if ( sqlCmd.hasConnection() && sqlDrus.getCompleter()!=null )
                            ((SqlDrusSyntaxCompleter) sqlDrus.getCompleter()).setAutocompleteObjects( sqlCmd.getAutocompleteObjects() );
                    } else printCommandHelp(cmd);
                } else if ("DESCRIBE".equalsIgnoreCase(cmd) || "DESC".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters )  while ( st.hasMoreTokens() ) sqlCmd.getDialect().describe(st.nextToken() );
                        else printCommandHelp(cmd);
                    }
                } else if ("INDEXES".equalsIgnoreCase(cmd) || "INDE".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters ) while ( st.hasMoreTokens() ) sqlCmd.getDialect().describeIndexes(st.nextToken() );
                        else printCommandHelp(cmd);
                    }
                } else if ("CONSTRAINTS".equalsIgnoreCase(cmd) || "CONSTR".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters ) while ( st.hasMoreTokens() ) sqlCmd.getDialect().describeConstraints(st.nextToken());
                        else printCommandHelp(cmd);
                    }
                } else if ("LS".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listObjectByType(ObjectTypes.ALL, null );
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listObjectByType(ObjectTypes.ALL, st.nextToken());
                } else if ("LT".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listTables(null);
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listTables(st.nextToken());
                } else if ("LV".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.", Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listViews(null);
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listViews(st.nextToken());
                } else if ("LQ".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.", Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listSequences(null);
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listSequences(st.nextToken());
                } else if ("LP".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.", Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listSynonyms(null);
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listSynonyms(st.nextToken());
                } else if ("LX".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.", Verbose.ERROR);
                    else if ( !hasParameters )  sqlCmd.getDialect().listProcedures(null);
                    else while ( st.hasMoreTokens() ) sqlCmd.getDialect().listProcedures(st.nextToken());
                } else if ("SRC".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else if ( hasParameters ) while ( st.hasMoreElements() ) sqlCmd.getDialect().sourceProcedure(st.nextToken());
                    else printCommandHelp(cmd);
                } else if ("TRIG".equalsIgnoreCase(cmd) || "TRIGGER".equalsIgnoreCase(cmd) ){
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else if ( hasParameters ) while ( st.hasMoreElements() ) sqlCmd.getDialect().sourceTrigger(st.nextToken());
                    else printCommandHelp(cmd);
                } else if ("SETENV".equalsIgnoreCase(cmd) ){
                    if ( hasParameters ){
                        String param = st.nextToken();
                        if ( param.equalsIgnoreCase("VERBOSE") ){
                            if ( st.hasMoreTokens() ) sqlCmd.setVerboseOutputString( st.nextToken() );
                            else sqlCmd.output("Verbose level: "+sqlCmd.getVerboseOutputString());
                        } else if (param.equalsIgnoreCase("AUTOCOMMIT") ){
                            if ( st.hasMoreTokens() ) sqlCmd.getDialect().setAutoCommit(st.nextToken());
                            else sqlCmd.output("Autocommit: "+sqlCmd.getDialect().getAutoCommit());
                        } else if (param.equalsIgnoreCase("ONERROR") ){
                            if ( st.hasMoreTokens() ) sqlCmd.setOnError(st.nextToken());
                            else sqlCmd.output("OnError: "+sqlCmd.getOnError());
                        } else if (param.equalsIgnoreCase("EDITOR") ){
                            if ( st.hasMoreTokens() ) setEditor( untoken(st) );
                            else sqlCmd.output("Editor: "+getEditor());
                        } else if (param.equalsIgnoreCase("TIMING") ){
                            if ( st.hasMoreTokens() ) {
                                setTiming( st.nextToken() );
                                startTime = System.currentTimeMillis();
                            } else
                                sqlCmd.output("Timing: "+getTiming());
                        } else if (param.equalsIgnoreCase("MAX_COL_SIZE") ){
                            if ( st.hasMoreTokens() ) {
                                sqlCmd.setMaxColSize( Integer.parseInt(st.nextToken()) );
                            } else
                                sqlCmd.output("MAX_COL_SIZE: "+sqlCmd.getMaxColSize());
                        } else if (param.equalsIgnoreCase("BULK_SIZE") ){
                            if ( st.hasMoreTokens() ) {
                                sqlCmd.setBulkSize( Integer.parseInt(st.nextToken()) );
                            } else
                                sqlCmd.output("BULK_SIZE: "+sqlCmd.getBulkSize());
                        } else {
                            printCommandHelp(cmd);
                        }
                    } else {
                        printCommandHelp(cmd);
                    }
                } else if ("SETVAR".equalsIgnoreCase(cmd) ){
                    String varName=null;
                    String dataType=null;
                    String varValue = null;
                    if ( !hasParameters ){
                        sqlCmd.printVariables();
                    } else {
                        while ( st.hasMoreTokens() ){
                            final String token = st.nextToken();
                            if ( varName == null ){
                                if ( !token.matches("\\w+") ){
                                    sqlCmd.output("Invalid variable name.",Verbose.ERROR);
                                    break;
                                }
                                varName=token;
                            } else if ( varName!=null && dataType == null ){
                                if ( !SUPPORTED_DATA_TYPES.contains(token.toUpperCase()) ){
                                    sqlCmd.output("Invalid datatype",Verbose.ERROR);
                                    break;
                                }
                                dataType = token.toUpperCase();
                            }
                            if ( varName !=null && dataType!=null ){
                                varValue = st.nextToken("").substring(1); // take the whole string and exit
                                break;
                            }
                        }
                        if ( varName == null || dataType == null ){
                            printCommandHelp(cmd);
                        } else {
                            // check for null literal
                            if ( varValue !=null && varValue.equalsIgnoreCase("NULL") ) varValue = null;
                            sqlCmd.setVariable(varName, dataType, varValue);
                        }
                    }
                } else if ( "RELOAD".equalsIgnoreCase(cmd) ){
                    if ( !hasParameters ){
                        sqlCmd.connectionLoad();
                    } else {
                        printCommandHelp(cmd);
                    }
                } else if ("DUMP".equalsIgnoreCase(cmd)  ){
                    log.debug("DUMP");
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters ) {
                            String tableName = st.nextToken();
                            log.debug("DUMP "+tableName);
                            String sql = Pattern.compile("^dump [^ ]+\\s*", java.util.regex.Pattern.CASE_INSENSITIVE).matcher(command).replaceAll("");
                            sql = Pattern.compile("\\s*;\\s*", java.util.regex.Pattern.CASE_INSENSITIVE).matcher(sql).replaceAll("");

                            if( st.hasMoreTokens() ) sqlCmd.getDialect().dumpTable(tableName,sql);
                            else printCommandHelp(cmd);
                        } else printCommandHelp(cmd);
                    }
                } else if ("BIND".equalsIgnoreCase(cmd)  ){
                    log.debug("BIND");
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters ) {
                            String sql = "";
                            List<String> vars = new ArrayList<>();
                            while (st.hasMoreTokens() ){
                                String varName = st.nextToken();
                                if ( varName.equalsIgnoreCase("select") ){
                                    sql="select" + st.nextToken("");
                                } else {
                                    vars.add(varName);
                                }
                            }
                            log.debug("BIND "+vars);
                            sqlCmd.getDialect().bind(vars,sql);
                        } else printCommandHelp(cmd);
                    }
                } else if ("SPOOL".equalsIgnoreCase(cmd) ){
                    if (hasParameters){
                        String file = st.nextToken();
                        if (file.equalsIgnoreCase("OFF") || file.equalsIgnoreCase("CONSOLE")){
                            sqlCmd.setOutputStream( out );
                        } else {
                            try {
                                sqlCmd.setOutputStream(new PrintStream(new FileOutputStream(file)));
                            } catch (FileNotFoundException e){
                                sqlCmd.output("Spool file can not be created!",Verbose.ERROR);
                            }
                        }
                    } else {
                        printCommandHelp(cmd);
                    }
                } else if ("ED".equalsIgnoreCase(cmd) || "EDIT".equalsIgnoreCase(cmd)){
                    if ( hasParameters )
                        edit(st.nextToken(), lastCommand);
                    else
                        edit(BUFFER_FILE, lastCommand);
                } else if ("CD".equalsIgnoreCase(cmd) ){
                    if ( hasParameters ){
                        String val = st.nextToken();
                        cwd( val );
                    } else {
                        sqlCmd.output(cwd);
                    }
                } else if ("HELP".equalsIgnoreCase(cmd) ){
                    if ( hasParameters )
                        printCommandHelp(st.nextToken());
                    else
                        printCommandHelp(null);
                } else if ("VERSION".equalsIgnoreCase(cmd) ){
                    sqlCmd.output(Version.VERSION_MESSAGE);
                } else if ("HISTORY".equalsIgnoreCase(cmd) ){
                        sqlCmd.output("Not implemented",Verbose.ERROR);
//                        Vector history = new Vector();
//                        Readline.getHistory(history);
//                        for (int i=0; i<history.size(); i++){
//                            sqlCmd.output((String) history.get(i),Verbose.ALL);
//                        }
                } else if ("QUIT".equalsIgnoreCase(cmd) || "EXIT".equalsIgnoreCase(cmd) ){
                    if ( sqlCmd.hasConnection() ) sqlCmd.disconnectAll();
                    result = false;
                } else if ("COPY".equalsIgnoreCase(cmd)  ){
                    log.debug("COPY");
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else {
                        if ( hasParameters ) {
                            String tableName = st.nextToken();
                            log.debug("COPY "+tableName);

                            if( st.hasMoreTokens() ){
                                if (!st.nextToken().equalsIgnoreCase("into")){
                                    printCommandHelp(cmd);
                                } else {
                                    if ( st.hasMoreTokens() ){
                                        String connectionName = st.nextToken();
                                        if ( connectionName.endsWith(";")) connectionName=connectionName.substring(0,connectionName.length()-1);
                                        List<String> columns = sqlCmd.getDialect().getColumns(tableName);
                                        if ( columns.size() == 0  ){
                                            sqlCmd.output("Table "+tableName+" does not exist.",Verbose.ERROR);
                                        } else {
                                            String whereExpr = null;
                                            if ( st.hasMoreTokens() && st.nextToken().equalsIgnoreCase("where")){
                                                StringBuilder sb = new StringBuilder();
                                                while (st.hasMoreTokens()){
                                                    sb.append(" ");
                                                    sb.append(st.nextToken());
                                                }
                                                if ( sb.charAt( sb.length()-1 ) == ';' ) sb.deleteCharAt(sb.length()-1);
                                                whereExpr = sb.toString();
                                            }
                                            execute( composeTransferSql(tableName, connectionName, columns, whereExpr) );
                                        }
                                    } else {
                                        printCommandHelp(cmd);
                                    }
                                }
                            }
                            else printCommandHelp(cmd);
                        } else printCommandHelp(cmd);
                    }
                } else {
                    if ( !sqlCmd.hasConnection() ) sqlCmd.output("Not connected.",Verbose.ERROR);
                    else sqlCmd.doSQLCommand(command);
                }
                if (result && timing) sqlCmd.output("\nElapsed: " + (System.currentTimeMillis() - startTime) +" ms.",Verbose.ERROR);
            } else {
                sqlCmd.output("No command!",Verbose.ERROR);
                printCommandHelp(null);
            }
        } catch (Exception e){
            sqlCmd.error(e);
        }
        return result;
    }

    private String composeTransferSql(String tableName, String connectionName, List<String> columns, String whereExpr) {
        StringBuilder columnsFragment = new StringBuilder();
        StringBuilder bindFragment = new StringBuilder();
        for (int i=0; i<columns.size(); i++){
            String column = columns.get(i);
            // enclose column names matching keywords (select, desc, ...) with quotes
            if (sqlDrus.getKeywords().contains(column.toLowerCase())) columnsFragment.append('"').append(column).append('"');
            else columnsFragment.append(column);
            bindFragment.append(':').append(i+1);
            if ( (i + 1) < columns.size() ){
                columnsFragment.append(',');
                bindFragment.append(',');
            }
        }
        return "select "+columnsFragment
                +" from "+tableName
                + (whereExpr==null?"":" where "+whereExpr)
                +" intercept "+connectionName+" bulk"
                +" insert into  "+tableName
                +" ("+columnsFragment+")"+
                " values ("+bindFragment+");";
    }

    /**
    * Print command help.
    * @param cmd - the command to print help about. If null, print help for all commands
    */
    public void printCommandHelp( String cmd){
        if (cmd == null){ // show help for all commands
            for (int i=0; i<HELP_STRING.length; i++ ){
                sqlCmd.output(HELP_STRING[i][1]);
            }
        } else { // command is specified . print just its help
            for (int i=0; i<HELP_STRING.length; i++ ){
                if ( Pattern.compile(HELP_STRING[i][0], Pattern.CASE_INSENSITIVE).matcher(cmd).matches() ){
                    sqlCmd.output(HELP_STRING[i][1]);
                    break;
                }
            }
        }
    }


    /**
    * processes line.
    * @param line - the line to process
    * @return 0 - start reading new command
    *          1 - line added to command, read next line (multiline command)
    *          -1 - stop reading and exit
    */
    public int process(String line){
        if ( line == null) return 0;
        String activeDialect = sqlCmd.getDialect()==null?null:sqlCmd.getDialect().getDialectName();

        int res = 0;
        if ( !isCreateRoutine ){
            if (line.startsWith(COMMENT) || line.trim().length()==0) return res;
            line = line.trim();
        }
         //if line is empty or starts with comment string, skip it, and read more lines
        if (command == null){
            StringTokenizer st = new StringTokenizer(line);
            firstCommand = st.nextToken().toUpperCase();
            command = line;
            // do not assume semicolon delimiter for sql-drus commands
            if ( isSqlDrusCommand(firstCommand)  &&
                    !firstCommand.startsWith(EXEC_HOST_COMMAND) &&
                    !firstCommand.startsWith(EXEC_FILE_COMMAND) &&
                    !firstCommand.startsWith(LAST_COMMAND) ){
                if ( !execute(command) ) res = -1;
                command = null;
            } else{
                // handle create procedure and create function statements as multiline sql and expect end]
                if ( firstCommand.equalsIgnoreCase("CREATE") ) {
                    if ( "Informix".equals( activeDialect ) ){
                        String nextKeyword = st.nextToken().toUpperCase();
                        if ( "PROCEDURE".equalsIgnoreCase(nextKeyword) || "FUNCTION".equalsIgnoreCase(nextKeyword))
                            isCreateRoutine = true;
                    } else if  ( "Oracle".equals(activeDialect) || "DB2".equals(activeDialect) ){
                       isCreateRoutine = Pattern.compile("CREATE\\s+(OR REPLACE\\s+)?(PROCEDURE|FUNCTION|PACKAGE|TRIGGER).*").matcher(line.toUpperCase()).matches();
                    }
                }
                res = 1;
            }
        } else {
            if ( !(isCreateRoutine && line.trim().equals(LAST_COMMAND) &&
                    ("Oracle".equals(activeDialect) || "DB2".equals(activeDialect)) ) )
                command += ( "\n" + line );
            res = 1;
        }
//        log.debug(String.format("firstCommand: %s\nline: %s\ncommand %s\nisCreateRoutine: %s",firstCommand,line,command,isCreateRoutine));

        if (command != null){
            if ( command.equals(LAST_COMMAND) && lastCommand != null) command = lastCommand;

            if (command.startsWith(EXEC_FILE_COMMAND)){
                execFileCommand( line.substring(1) );
                command = null;
                res = 0;
            } else if (command.startsWith(EXEC_HOST_COMMAND)) {
                hostCommand(line.substring(1));
                command = null;
                res = 0;
            } else if ( line.equals(LAST_COMMAND) && isCreateRoutine &&
                        ("Oracle".equals(activeDialect) || "DB2".equals(activeDialect)) ) {
                sqlCmd.getDialect().setDoBindings(false);
                execute(command);
                sqlCmd.getDialect().setDoBindings(true);
                isCreateRoutine = false;
                lastCommand = command;
                command = null;
                res = 0;
            } else if (command.charAt(command.length()-1) ==  COMMAND_SEPARATOR){
                if ( isCreateRoutine && "Informix".equals(activeDialect) ) {
                    String upLine = line.trim().toUpperCase();
                    if (upLine.endsWith("END PROCEDURE;") || upLine.endsWith("END FUNCTION;")) {
                        sqlCmd.getDialect().setDoBindings(false);
                        execute(command);
                        sqlCmd.getDialect().setDoBindings(true);
                        isCreateRoutine = false;
                        lastCommand = command;
                        command = null;
                        res = 0;
                    } else res = 1;
                } else if (isCreateRoutine && ("Oracle".equals(activeDialect) || "DB2".equals(activeDialect)) ){
                    res = 1;
                } else if ( ! execute(command.substring(0,command.length()-1)) ) {
                    res = -1;
                } else {
                    lastCommand = command;
                    command = null;
                    res = 0;
                }
            }
        }
        return res;
    }

  /**
  * Executes a host commands. Gets the output of it
  * and prints on the console.
  */
   public void hostCommand(String line){
      try {
         // make this array to handle correctly the pipes in *nix environment
         String params[] = { shell,shell_args, line };
         Process proc = Runtime.getRuntime().exec(params, new String[]{}, new File(cwd) );
         BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
         String lineRead = null;
         while((lineRead = reader.readLine()) != null){
            sqlCmd.output(lineRead,Verbose.ERROR); //You can send output from here to a GUI display if needed
         }
      }catch(Exception e){
         sqlCmd.error(e);
      }
   }

  /**
  * Executes a commads from file.
  */
   public int execFileCommand(String file){
    int cpRes = 0;
    try {
        file = file.trim();
        if ( !file.startsWith(File.separator) ) file = cwd+File.separator+file;
        File execFile = new File( file );
        if (execFile.exists()){
            BufferedReader br = new BufferedReader( new FileReader(execFile) );
            String fLine = null;
            // recursively process each line
            command = null;
            firstCommand = null;
            while ( cpRes >=0 && (fLine = br.readLine()) != null ){
                sqlCmd.output("> "+fLine,Verbose.ALL);
                cpRes = process(fLine);
            }
            br.close();
        } else {
            sqlCmd.output("File "+execFile.getAbsolutePath()+" does not exists.",Verbose.ERROR);
        }
    } catch (IOException e) {
        sqlCmd.error(e);
    }
    return cpRes;
   }

  /**
  * Executes a specified editor to edit last valid dml or ddl command
  */
   public void edit(String file, String command){
    try {
        File fFile= new File( file );

        if ( file.equals(BUFFER_FILE) || !fFile.exists() ){ // file exists. edit it
            FileWriter fw = new FileWriter(fFile);
            fw.write( (command==null) ? "" : command );
            fw.close();
        }
        log.debug("exec: "+shell+" "+shell_args+" "+editor+" "+file );
        String params[] = { shell,shell_args, editor+" "+file };
        Process proc = Runtime.getRuntime().exec(params);
        int exitVal = proc.waitFor();
        if (exitVal == 0){
            execFileCommand(file);
        }
    } catch (IOException e) {
        sqlCmd.error(e);
    } catch (InterruptedException e) {
        sqlCmd.error(e);
    }
   }

  /**
  * Change current working directory to newdir
  */
   public void cwd(String newdir){
    try {
        String path = newdir;
        if ( newdir.startsWith(File.separator) ) path = newdir;
        else path = cwd + File.separator + newdir;

        File fPath= new File( path );
        if ( !fPath.exists() ) throw new IOException("Directory does not exists!");
        if ( !fPath.isDirectory() ) throw new IOException("Not a directory!");
        cwd = fPath.getCanonicalPath();
        ((SqlDrusSyntaxCompleter)sqlDrus.getCompleter()).setFilenameCompleterDir(cwd);
    } catch (IOException e) {
        sqlCmd.error(e);
    }
   }

    public void start(){
        if ( sqlDrus.getCompleter() !=null )
            ((SqlDrusSyntaxCompleter)sqlDrus.getCompleter()).setAutocompleteConnectionNames( sqlCmd.getConnectionNames() );
    }

    public void stop(){
    }


   /**
   * helper function to untokeze sting tokens
   */
   private String untoken( StringTokenizer st){
    String result ="";
    while (st.hasMoreTokens()){
        result += st.nextToken()+" ";
    }
    if ( result.length()>0 ) result = result.substring(0, result.length()-1); // remove last space
    return result;
   }

   private boolean isSqlDrusCommand(String cmd){
       for (String[] c: HELP_STRING){
           if (Pattern.compile(c[0],Pattern.CASE_INSENSITIVE).matcher(cmd).matches() )
               return true;
       }
       return false;
   }
}
