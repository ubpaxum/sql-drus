package org.ibro.sqldrus.completer;

import org.ibro.sqldrus.config.SqlDrusConfig;
import org.jline.reader.Candidate;
import org.jline.reader.impl.completer.StringsCompleter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/15/16
 * Time: 5:06 PM
 * Short description of purpose.
 */
public class SqlDrusCompleter extends StringsCompleter {
    //private static final Logger log = LoggerFactory.getLogger(SqlDrusCompleter.class);
    private final List<String> keywords;
    public SqlDrusCompleter(SqlDrusConfig config) {
        super();
        keywords = new ArrayList<>();
        try {
            load(SqlDrusCompleter.class.getResourceAsStream("/" + SqlDrusConfig.KEYWORDS_FILE));
            for (String f: config.getKeywordsFiles() ){
                    InputStream is = new FileInputStream(f);
                    load( is );
                    is.close();
            }
        } catch (IOException e) {
            //log.error("Error", e);
        }
    }

    private void load(InputStream stream) throws IOException {
        BufferedReader r = new BufferedReader( new InputStreamReader(stream) );
        String line;
        while ( (line=r.readLine()) != null ){
            candidates.add( new Candidate(line) );
            keywords.add(line.trim().toLowerCase());
        }
    }

    public List<String> getKeywords() {
        return keywords;
    }
}
