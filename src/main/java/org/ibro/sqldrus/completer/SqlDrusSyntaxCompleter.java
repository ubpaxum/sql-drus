package org.ibro.sqldrus.completer;

import org.ibro.sqldrus.config.SqlDrusConfig;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.jline.reader.impl.completer.*;
import org.jline.utils.AttributedString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ibro
 * Date: 12/15/16
 * Time: 5:06 PM
 * Short description of purpose.
 */
public class SqlDrusSyntaxCompleter implements Completer {
    private static final Logger log = LoggerFactory.getLogger(SqlDrusCompleter.class);
    private AggregateCompleter completer;
//    private ObjectNameCompleter objectCompleter = null;
    Map<String,Completer> commands = new HashMap<String, Completer>();
    private MyFilenameCompleter filenameCompleter;

    private List<Candidate> objectNames = new ArrayList<Candidate>();
    private List<Candidate> connectionNames = new ArrayList<Candidate>();

    public SqlDrusSyntaxCompleter(SqlDrusConfig config) {
        try {
            this.filenameCompleter = new MyFilenameCompleter( System.getProperty("user.dir") );
            completer = load(SqlDrusSyntaxCompleter.class.getResourceAsStream("/" + config.SYNTAX_FILE));
        } catch (IOException e) {
            log.error("Error", e);
        }
    }

    @Override
    public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
        int idx = line.wordIndex();
        if ( idx < 1 )  {
            completer.complete(reader,line,candidates);
        }  else {
            String firstCommand = line.words().get(0).toLowerCase();
            Completer c = commands.get(firstCommand);
            if ( c !=null  )  {
                c.complete(reader,line,candidates);
            }
        }
    }

    private AggregateCompleter load(InputStream stream) throws IOException {
        List<Completer> completers = new LinkedList<Completer>();
        String line;
        BufferedReader r = new BufferedReader( new InputStreamReader(stream) );
        while ( (line=r.readLine()) != null  ){
            String[] words =  line.split(" ");

            if ( words.length < 1 ) continue; // skip empty lines
            int startIndex = 1;
            String completerType = "NullCompleter";
            String keyword = words[0];
            if ( words[0].endsWith(":") ){ // completer defined
                completerType = words[0].substring(0,words[0].length()-1);
                keyword = words[1];
                startIndex = 2;
            }
            Completer argumentCompleter = createCompleter(completerType,keyword, Arrays.copyOfRange(words,startIndex,words.length) );
            completers.add( argumentCompleter );
            commands.put(keyword,argumentCompleter);
        }
        return new AggregateCompleter( completers );
    }

    public void setAutocompleteObjects(List<String> objects){
        objectNames.clear();
        for (String o: objects){
            objectNames.add( new Candidate(AttributedString.stripAnsi(o), o, null, null, null, null, true) );
        }
    }

    public void setAutocompleteConnectionNames(List<String> objects){
        connectionNames.clear();
        for (String o: objects){
            connectionNames.add( new Candidate(AttributedString.stripAnsi(o), o, null, null, null, null, true) );
        }
    }


    private Completer createCompleter(String type, String command, String... args){
        Completer c;
        if ( type.equals("NullCompleter") ){
            c = new NullCompleter();
        } else if ( type.equals("ObjectNameCompleter") ){
            c = new ObjectNameCompleter(args);
        } else if ( type.equals("FileNameCompleter") ){
            c = filenameCompleter;
        } else if ( type.equals("ConnectionNameCompleter") ){
            c = new ConnectionNameCompleter();
        } else if ( type.equals("StringsCompleter") ){
            c = new StringsCompleter(args);
        } else {
            c = new NullCompleter();
        }
        ArgumentCompleter res = new ArgumentCompleter( new StringsCompleter(command), c);
        res.setStrict(false);
        return res;
    }

    class ObjectNameCompleter implements Completer {
        private final List<Candidate> keywords = new ArrayList<Candidate>();

        public ObjectNameCompleter() {
        }

        public ObjectNameCompleter(String... strings) {
            this(Arrays.asList(strings));
        }

        public ObjectNameCompleter(List<String> strings) {
            for (String string : strings) {
                Candidate c = new Candidate(AttributedString.stripAnsi(string), string, null, null, null, null, true);
                keywords.add(c);
            }
        }

        public void complete(LineReader reader, final ParsedLine commandLine, final List<Candidate> candidates) {
            candidates.addAll(this.keywords);
            candidates.addAll( objectNames );
        }
    }

    class MyFilenameCompleter extends FileNameCompleter{
        private String cwd;

        public MyFilenameCompleter(String cwd) {
            super();
            this.cwd = cwd;
        }

        @Override
        protected Path getUserDir() {
            return Paths.get( cwd );
        }

        public String getCwd() {
            return cwd;
        }

        public void setCwd(String cwd) {
            log.info("Set filename completer dir {} -> {}", this.cwd, cwd);
            this.cwd = cwd;
        }
    }

    class ConnectionNameCompleter extends StringsCompleter {
        public void complete(LineReader reader, final ParsedLine commandLine, final List<Candidate> candidates) {
            if (connectionNames!=null) candidates.addAll( connectionNames );
        }
    }

    public void setFilenameCompleterDir(String dir){
        log.info("setFilenameCompleterDir {}", dir);
        filenameCompleter.setCwd(dir);
    }

}
