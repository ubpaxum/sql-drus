commit 8f02d2edefa15ef50710b289d83845cfa7dfaec9
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 12:31:24 2016 +0200

    If active connection is disconnected, switch to next active connection (if any).

commit 3c6dc8a288b5bb35d861b3ee9a8033b57fac78da
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 12:23:31 2016 +0200

    Bugfix: disconnect without arguments will disconnect active connection.

commit a6b298272f2f79700a8bfa6218c5736ffa4532af
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 11:50:14 2016 +0200

    Colorize sql-drus prompt.

commit f0198cb80c3abf84b29a84b68f914736c663b79b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:55:12 2016 +0200

    Put history file in user's home

commit 6ae619d7462ac94292f65593d05efa4b65c48805
Merge: 04d4e28 34b1a49
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:50:09 2016 +0200

    Merge branch 'interceptor' and 'jline'

commit 04d4e2890ee4fea718fdab00ac4ca2dc6211acfa
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:38:51 2016 +0200

    Use jLine java library as replacement of rlwrap - this will make sql-drus self-sustaining and independent from external products.

commit 34b1a49dd8c26a9c9fdb2697bf0bea2935a5235d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:50:13 2016 +0200

    Update README with command syntax.

commit 02241d0ca72b822fdaea08b524846035359f921b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:49:52 2016 +0200

    Bugfix: use list (not set) for named parameter to preserve parameters order.

commit 6954a55af85d46b8cf4b21c174939de4d9329cbb
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:49:16 2016 +0200

    Introduce smallint type in bind variables

commit ab912be9d853cdb748a30ac4dcbd04015684bf65
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 14:35:37 2016 +0200

    Code refactoring: split classes in packages

commit a9d078f0228bc39fcec9a2f317570b2ded6b6a73
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 12:25:45 2016 +0200

    Handle multiple intercept clauses.

commit 96fd57cb5afc100211041cd44678552e44c29a9a
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 11:24:16 2016 +0200

    Introducing interceptors to handle cross-database queries with "intercept <connectionName>" with multiple active connections to different databases.
    Keep connection parameter added to connect command (--keep flag)
    Optional connectionName parameter to disconnect command
    New sql-drus commands to work with multiple connections
     * switch <connectionName>
     * showconn - prints active connection

commit 022aeae68c43ca8d1be150c0f38b7efd08bf0932
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Dec 12 15:39:59 2016 +0200

    Refactoring: create object type enum instead of hard-coded values.

commit 812ad62e4dcc39e013fc20b574f915f5f721a76b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 9 15:47:42 2016 +0200

    Do not hardcode SQLDRUS_HOME in startup script.
    Configure installDist task

commit 4a26c91f8f0a02ce6cac9c3b2fec37809c9b44c0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 9 09:54:33 2016 +0200

    Small fixes for command parsing.

commit 13c2b076ff622852187d28b6459a6778e3f7c4a0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 8 12:52:22 2016 +0200

    Add support for DB2 (tested with DB2 v 11.1 which contains Oracle like data dictionionary).

commit 8536b990b40abc676f166ec0fc97385eeef4f891
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Dec 5 12:14:59 2016 +0200

    Add keywords to be used by rlwrap for autocompletion.'

commit 013e29840126e6528b4659a1ada2f406af46fd86
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 15:58:51 2016 +0000

    Update README

commit 2949003e717716b732dd04c6472f8786d9691833
Merge: c89ddae d198f99
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:49:48 2016 +0200

    Merge branch 'refactor_multiple_db'
    
    Conflicts:
        ChangeLog.txt

commit c89ddae37fb8055a0d4537761db369aba9a8964b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:47:56 2016 +0200

    add todo.

commit d198f998f4c45337734e1dfc73858c99ba35e642
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:45:51 2016 +0200

    add todo.

commit 9e8213bad4af488a703288feb17e7a66d807a268
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:42:33 2016 +0200

    Keep logging to INFO.

commit 2f6941fb9bdce6b0941676376c502dfe9718dbaa
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 12:30:53 2016 +0200

    Fix DDL statements to be executed with Statment and not bindings.
    Fix command parser to work with Oracle for procedure, function, package and trigger creation DDLs.

commit 78134a38367cd8e5f5433158c6dd98afe72cc8d3
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 29 10:11:40 2016 +0200

    Add sample connection string for Oracle db.

commit e75a2217f3bb310fb083495c89e05543a7df8470
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 29 10:11:24 2016 +0200

    Fix trigger source for Oracle db.

commit 190ffad7c8e3d2c5e86af22dee82eb23ee135e3f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Nov 28 18:49:14 2016 +0200

    More improvements on oracle support.

commit b71e4d87e88e4a0eb15f79267debce3f1e7b80a8
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Nov 28 17:20:53 2016 +0200

    Main refactoring to support multiple databases - added support for Oracle database.

commit 0cd4935f95d133c458f395c06fecf496a7fb4a94
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 14:13:46 2016 +0200

    Get version from gradle build file

commit 5a29b8de9709988c4afc7f14bedec8853ae47378
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 11:57:39 2016 +0200

    Add version information and fix verbosity outputs.

commit 89ad433224b95a4884d421399491f85a9359af52
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 11:22:33 2016 +0200

    Use regular expression to proper determine SQLDRUS_HOME

commit 8ad872ff6979d32cfd16f65e8d578b56f8e93db0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 24 17:41:46 2016 +0200

    Determine SQLDRUS_HOME from jar file if not set in environment. Log file is put in SQLDRUS_HOME/log directory.

commit 2c2f3dabf66158f454aa4fe70798e45816f4e46f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 16 14:11:04 2016 +0200

    Bugfix: check how the first command start to handle specials host and exec commands.

commit 23af701f39a5eec5164255b3fc589240d10f59f9
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 15 13:45:49 2016 +0200

    Bugfix: don't skip the first row - next fetched. not only checks.

commit b8065f0bf127f0be786c8ffdee584dc0db213993
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 11 11:06:52 2016 +0200

    DESC command can also work with pattern, not only with full table name.

commit 33fb68bca493465041654838ee97976c5579184c
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 11 10:19:19 2016 +0200

    Use and process all paramaters of sqldrus commands, not only the first one.

commit c7476e94ae12401c72997a3bb3f29543e7bb373b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:59:30 2016 +0200

    Really update README.

commit eb604bdab9534aefd42d85deb395b7621472635f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:32:33 2016 +0200

    Update README

commit f1e53c0168caf60190205f44d2839dbb98a4f266
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:32:22 2016 +0200

    Add TRIG[GER] [owner.]{table} command to show triggers for specified tables.

commit 899c05d174a75437015d6f3cf0f453bb34df0b7c
Merge: e55f17f 99c21bc
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 13:38:12 2016 +0200

    Merge branch 'master' of gitlab.com:ubpaxum/sql-drus

commit e55f17f492a0c13b243af2519d7a6304cf0a5275
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 13:36:28 2016 +0200

    Update changelog.

commit 99c21bcdc4164706c0548dab5637dfed37405c5d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:33:52 2016 +0000

    Update README

commit eaac9e2567ae4750bce6243cfc1d7a86a8240306
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:52:31 2016 +0200

    Use Set to store configuration directory to remove duplicates in list.

commit 476f4502c3bd791c98832c064681298a4492f171
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:44:30 2016 +0200

    Code refactoring for better readability.

commit 9b95e6c99155d471b0fae320dfc783f8f3d34332
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:26:08 2016 +0200

    Move SQL constants to interface

commit bcb23904cd900d8ca7457f18fd3eb311d1bb8120
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 10:38:48 2016 +0200

    Add 'lx' command to list stored procedures/functions and 'src <procname>' to dump stored procedure source code.

commit d20c74366fbd55f05d74696faa0dfb2a7655a49b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:43:02 2016 +0200

    Cleanup unused libreadline

commit ab24cec4e83335a3d90737c5d89a5de3f0655ce3
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:28:13 2016 +0200

    sql-drus can reads the standart input in non-interactive mode (i.e. when piping). So you can use it like echo -e "conn soacustoms\nls *risk*\n" | sql-drus

commit 59c364a7151dfde4938b955345bf9487a2b057f6
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:27:03 2016 +0200

    Use regular expressions to match sql-drus commands.

commit 370bfcd3daba373d872ad7fd2d1b5c41c7b92c7d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:26:16 2016 +0200

    ls, lt, lv, lq, lp work with patterns - i.e. ls trn* will give you list of all objects starting with trn*

commit f4e82685e04b6d6450cf9adafd71be558a8851df
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 16:25:08 2016 +0200

    Implement more sql commands to list user objects (ls -all, lt - tables, lv - views, lq - sequences and lp - synonyms)

commit a015b37359387ba8aa6fae6c6babab5125021eae
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 14:07:15 2016 +0200

    Make 1.2 version with no libreadline-java dependency.

commit b03b90199fca706f12daee960785d7bd8f1d2356
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 14:04:21 2016 +0200

    Remove libreadline-java as it has not changed since 2003.

commit f94aa20c44751bcc8e3dd5570837e7e143ff302d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 13:16:12 2016 +0200

    Update the changelog.

commit 7f64f51dd5d3f6363c5f689d488fd70a98687be6
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 11:53:49 2016 +0200

    Refactor sql-drus configuration

commit cb314fb00e749beafc99023f5f1394889b6ccab8
Merge: d74f3ce 19691c7
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 08:59:02 2016 +0000

    Merge branch 'merge-test' into 'master'
    
    Update changelog to test merge.
    
    Merge it
    
    See merge request !2

commit 19691c7d991277eb0ad6f2b9143fed6409534f52
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 10:53:52 2016 +0200

    Update changelog to test merge.

commit d74f3ce248b8098d8c3d6c211e1ad8d75d7d64ea
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 10:13:05 2016 +0200

    small fixes with distribution and logging.

commit b6f9fb3122b63e8077e99691dcbda39c3a3ee6ed
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 18:09:57 2016 +0200

    Migrate to slf4j using log4j backend.

commit 6cc74d93ddfc9f32f44e2dd266dcd8dee3bc24db
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:57:17 2016 +0200

    Update readme.

commit 33b7d703e2b9967746be1dd84be717b7958a0cdb
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:47:48 2016 +0200

    Complete migration.

commit 47f9926c6326d250f8e8110f05df2257273ca055
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:45:04 2016 +0200

    Migrate to gradle and Java8

commit ca92abe3111ec5e036138a1ddf94029249493129
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 14:45:27 2016 +0200

    Initial import from Subversion
commit 8f02d2edefa15ef50710b289d83845cfa7dfaec9
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 12:31:24 2016 +0200

    If active connection is disconnected, switch to next active connection (if any).

commit 3c6dc8a288b5bb35d861b3ee9a8033b57fac78da
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 12:23:31 2016 +0200

    Bugfix: disconnect without arguments will disconnect active connection.

commit a6b298272f2f79700a8bfa6218c5736ffa4532af
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 16 11:50:14 2016 +0200

    Colorize sql-drus prompt.

commit f0198cb80c3abf84b29a84b68f914736c663b79b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:55:12 2016 +0200

    Put history file in user's home

commit 6ae619d7462ac94292f65593d05efa4b65c48805
Merge: 04d4e28 34b1a49
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:50:09 2016 +0200

    Merge branch 'interceptor' and 'jline'

commit 04d4e2890ee4fea718fdab00ac4ca2dc6211acfa
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 17:38:51 2016 +0200

    Use jLine java library as replacement of rlwrap - this will make sql-drus self-sustaining and independent from external products.

commit 34b1a49dd8c26a9c9fdb2697bf0bea2935a5235d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:50:13 2016 +0200

    Update README with command syntax.

commit 02241d0ca72b822fdaea08b524846035359f921b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:49:52 2016 +0200

    Bugfix: use list (not set) for named parameter to preserve parameters order.

commit 6954a55af85d46b8cf4b21c174939de4d9329cbb
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 15:49:16 2016 +0200

    Introduce smallint type in bind variables

commit ab912be9d853cdb748a30ac4dcbd04015684bf65
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 14:35:37 2016 +0200

    Code refactoring: split classes in packages

commit a9d078f0228bc39fcec9a2f317570b2ded6b6a73
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 12:25:45 2016 +0200

    Handle multiple intercept clauses.

commit 96fd57cb5afc100211041cd44678552e44c29a9a
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 15 11:24:16 2016 +0200

    Introducing interceptors to handle cross-database queries with "intercept <connectionName>" with multiple active connections to different databases.
    Keep connection parameter added to connect command (--keep flag)
    Optional connectionName parameter to disconnect command
    New sql-drus commands to work with multiple connections
     * switch <connectionName>
     * showconn - prints active connection

commit 022aeae68c43ca8d1be150c0f38b7efd08bf0932
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Dec 12 15:39:59 2016 +0200

    Refactoring: create object type enum instead of hard-coded values.

commit 812ad62e4dcc39e013fc20b574f915f5f721a76b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 9 15:47:42 2016 +0200

    Do not hardcode SQLDRUS_HOME in startup script.
    Configure installDist task

commit 4a26c91f8f0a02ce6cac9c3b2fec37809c9b44c0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Dec 9 09:54:33 2016 +0200

    Small fixes for command parsing.

commit 13c2b076ff622852187d28b6459a6778e3f7c4a0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Dec 8 12:52:22 2016 +0200

    Add support for DB2 (tested with DB2 v 11.1 which contains Oracle like data dictionionary).

commit 8536b990b40abc676f166ec0fc97385eeef4f891
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Dec 5 12:14:59 2016 +0200

    Add keywords to be used by rlwrap for autocompletion.'

commit 013e29840126e6528b4659a1ada2f406af46fd86
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 15:58:51 2016 +0000

    Update README

commit 2949003e717716b732dd04c6472f8786d9691833
Merge: c89ddae d198f99
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:49:48 2016 +0200

    Merge branch 'refactor_multiple_db'
    
    Conflicts:
        ChangeLog.txt

commit c89ddae37fb8055a0d4537761db369aba9a8964b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:47:56 2016 +0200

    add todo.

commit d198f998f4c45337734e1dfc73858c99ba35e642
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:45:51 2016 +0200

    add todo.

commit 9e8213bad4af488a703288feb17e7a66d807a268
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 14:42:33 2016 +0200

    Keep logging to INFO.

commit 2f6941fb9bdce6b0941676376c502dfe9718dbaa
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 30 12:30:53 2016 +0200

    Fix DDL statements to be executed with Statment and not bindings.
    Fix command parser to work with Oracle for procedure, function, package and trigger creation DDLs.

commit 78134a38367cd8e5f5433158c6dd98afe72cc8d3
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 29 10:11:40 2016 +0200

    Add sample connection string for Oracle db.

commit e75a2217f3bb310fb083495c89e05543a7df8470
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 29 10:11:24 2016 +0200

    Fix trigger source for Oracle db.

commit 190ffad7c8e3d2c5e86af22dee82eb23ee135e3f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Nov 28 18:49:14 2016 +0200

    More improvements on oracle support.

commit b71e4d87e88e4a0eb15f79267debce3f1e7b80a8
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Mon Nov 28 17:20:53 2016 +0200

    Main refactoring to support multiple databases - added support for Oracle database.

commit 0cd4935f95d133c458f395c06fecf496a7fb4a94
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 14:13:46 2016 +0200

    Get version from gradle build file

commit 5a29b8de9709988c4afc7f14bedec8853ae47378
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 11:57:39 2016 +0200

    Add version information and fix verbosity outputs.

commit 89ad433224b95a4884d421399491f85a9359af52
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 25 11:22:33 2016 +0200

    Use regular expression to proper determine SQLDRUS_HOME

commit 8ad872ff6979d32cfd16f65e8d578b56f8e93db0
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 24 17:41:46 2016 +0200

    Determine SQLDRUS_HOME from jar file if not set in environment. Log file is put in SQLDRUS_HOME/log directory.

commit 2c2f3dabf66158f454aa4fe70798e45816f4e46f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 16 14:11:04 2016 +0200

    Bugfix: check how the first command start to handle specials host and exec commands.

commit 23af701f39a5eec5164255b3fc589240d10f59f9
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 15 13:45:49 2016 +0200

    Bugfix: don't skip the first row - next fetched. not only checks.

commit b8065f0bf127f0be786c8ffdee584dc0db213993
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 11 11:06:52 2016 +0200

    DESC command can also work with pattern, not only with full table name.

commit 33fb68bca493465041654838ee97976c5579184c
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Fri Nov 11 10:19:19 2016 +0200

    Use and process all paramaters of sqldrus commands, not only the first one.

commit c7476e94ae12401c72997a3bb3f29543e7bb373b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:59:30 2016 +0200

    Really update README.

commit eb604bdab9534aefd42d85deb395b7621472635f
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:32:33 2016 +0200

    Update README

commit f1e53c0168caf60190205f44d2839dbb98a4f266
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 16:32:22 2016 +0200

    Add TRIG[GER] [owner.]{table} command to show triggers for specified tables.

commit 899c05d174a75437015d6f3cf0f453bb34df0b7c
Merge: e55f17f 99c21bc
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 13:38:12 2016 +0200

    Merge branch 'master' of gitlab.com:ubpaxum/sql-drus

commit e55f17f492a0c13b243af2519d7a6304cf0a5275
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 13:36:28 2016 +0200

    Update changelog.

commit 99c21bcdc4164706c0548dab5637dfed37405c5d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:33:52 2016 +0000

    Update README

commit eaac9e2567ae4750bce6243cfc1d7a86a8240306
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:52:31 2016 +0200

    Use Set to store configuration directory to remove duplicates in list.

commit 476f4502c3bd791c98832c064681298a4492f171
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:44:30 2016 +0200

    Code refactoring for better readability.

commit 9b95e6c99155d471b0fae320dfc783f8f3d34332
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 11:26:08 2016 +0200

    Move SQL constants to interface

commit bcb23904cd900d8ca7457f18fd3eb311d1bb8120
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Thu Nov 10 10:38:48 2016 +0200

    Add 'lx' command to list stored procedures/functions and 'src <procname>' to dump stored procedure source code.

commit d20c74366fbd55f05d74696faa0dfb2a7655a49b
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:43:02 2016 +0200

    Cleanup unused libreadline

commit ab24cec4e83335a3d90737c5d89a5de3f0655ce3
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:28:13 2016 +0200

    sql-drus can reads the standart input in non-interactive mode (i.e. when piping). So you can use it like echo -e "conn soacustoms\nls *risk*\n" | sql-drus

commit 59c364a7151dfde4938b955345bf9487a2b057f6
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:27:03 2016 +0200

    Use regular expressions to match sql-drus commands.

commit 370bfcd3daba373d872ad7fd2d1b5c41c7b92c7d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 17:26:16 2016 +0200

    ls, lt, lv, lq, lp work with patterns - i.e. ls trn* will give you list of all objects starting with trn*

commit f4e82685e04b6d6450cf9adafd71be558a8851df
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 16:25:08 2016 +0200

    Implement more sql commands to list user objects (ls -all, lt - tables, lv - views, lq - sequences and lp - synonyms)

commit a015b37359387ba8aa6fae6c6babab5125021eae
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 14:07:15 2016 +0200

    Make 1.2 version with no libreadline-java dependency.

commit b03b90199fca706f12daee960785d7bd8f1d2356
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 14:04:21 2016 +0200

    Remove libreadline-java as it has not changed since 2003.

commit f94aa20c44751bcc8e3dd5570837e7e143ff302d
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 13:16:12 2016 +0200

    Update the changelog.

commit 7f64f51dd5d3f6363c5f689d488fd70a98687be6
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 11:53:49 2016 +0200

    Refactor sql-drus configuration

commit cb314fb00e749beafc99023f5f1394889b6ccab8
Merge: d74f3ce 19691c7
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 08:59:02 2016 +0000

    Merge branch 'merge-test' into 'master'
    
    Update changelog to test merge.
    
    Merge it
    
    See merge request !2

commit 19691c7d991277eb0ad6f2b9143fed6409534f52
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 10:53:52 2016 +0200

    Update changelog to test merge.

commit d74f3ce248b8098d8c3d6c211e1ad8d75d7d64ea
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Wed Nov 9 10:13:05 2016 +0200

    small fixes with distribution and logging.

commit b6f9fb3122b63e8077e99691dcbda39c3a3ee6ed
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 18:09:57 2016 +0200

    Migrate to slf4j using log4j backend.

commit 6cc74d93ddfc9f32f44e2dd266dcd8dee3bc24db
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:57:17 2016 +0200

    Update readme.

commit 33b7d703e2b9967746be1dd84be717b7958a0cdb
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:47:48 2016 +0200

    Complete migration.

commit 47f9926c6326d250f8e8110f05df2257273ca055
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 17:45:04 2016 +0200

    Migrate to gradle and Java8

commit ca92abe3111ec5e036138a1ddf94029249493129
Author: Ibrahim Tarla <ubpaxum@gmail.com>
Date:   Tue Nov 8 14:45:27 2016 +0200

    Initial import from Subversion

Add SqlDrusConfig - central configuration class

Migrated to git without import Subversion history
------------------------------------------------------------------------

------------------------------------------------------------------------
r18 | ibro | 2005-07-05 13:09:40 +0300 (��, 05 ��� 2005) | 3 lines

 * execute procedure | function is treated as SELECT statement since it can return result set
 * DESCRIBE commands show default values of columns

------------------------------------------------------------------------
r17 | ibro | 2005-06-21 17:21:29 +0300 (��, 21 ��� 2005) | 3 lines

 * History command added to show command line history
 * some fixes regaurding create procedude and create function syntaxis

------------------------------------------------------------------------
r16 | ibro | 2005-06-16 16:33:58 +0300 (��, 16 ��� 2005) | 5 lines

 * -- comment on files added
 * sql-drus.startup - this file will be executed in startup of sql-drus (if exists)
 * ED[IT] [file_name] - edits file or current buffer in external text editor. After finish of editing buffer will be loaded in sql-drus and executed.


------------------------------------------------------------------------
r15 | ibro | 2005-06-15 18:45:38 +0300 (��, 15 ��� 2005) | 2 lines

Implemented the '!'host_command 

------------------------------------------------------------------------
r14 | ibro | 2005-06-14 16:53:03 +0300 (��, 14 ��� 2005) | 1 line

Better format output from SELECT command
------------------------------------------------------------------------
r13 | ibro | 2005-06-14 15:41:08 +0300 (��, 14 ��� 2005) | 4 lines

Feature add: 
  * SETENV TIMING ON | OFF - to meter elapsed time for execution
  * If third argument of SETVAR is not specified, it will show the current value of 2-nd parameter (if valid one)

------------------------------------------------------------------------
r12 | ibro | 2005-06-14 07:28:41 +0300 (��, 14 ��� 2005) | 1 line

Bug fix in DESCRIBE command and better error handling.
------------------------------------------------------------------------
r11 | ibro | 2005-06-14 06:45:41 +0300 (��, 14 ��� 2005) | 1 line

new SETVAR setting AUTOCOMMIT - on/off. New structure of help string.
------------------------------------------------------------------------
r10 | ibro | 2005-06-10 14:15:20 +0300 (��, 10 ��� 2005) | 1 line

property svn:ignore to jar and log files
------------------------------------------------------------------------
r9 | ibro | 2005-06-10 14:12:05 +0300 (��, 10 ��� 2005) | 1 line

property svn:ignore to jar and log files
------------------------------------------------------------------------
r8 | ibro | 2005-06-10 11:24:23 +0300 (��, 10 ��� 2005) | 3 lines

Bugfix when displaying size of DECIMAL and LOB columns.


------------------------------------------------------------------------
r7 | ibro | 2005-06-08 17:13:45 +0300 (��, 08 ��� 2005) | 5 lines

1./ Support of tab completion added. Commans are loaded from configuration file sql-drus.dictionary - one command per line.
2./ New command SETENV to set environment veriables. Currently support variable VERBOSE with levels INFO | SILENT | ERROR | DEBUG. 
3./ Bugfix in output procedure


------------------------------------------------------------------------
r6 | ibro | 2005-06-08 11:09:56 +0300 (��, 08 ��� 2005) | 4 lines

@file - command added - to execute commands from file with sample script file
LD_LIBRARY_PATH to be set in sql-drus script


------------------------------------------------------------------------
r5 | ibro | 2005-06-08 10:17:48 +0300 (��, 08 ��� 2005) | 3 lines

Code reorganization. Some functionality for parsing lines and command moved to new class - CommandParser.


------------------------------------------------------------------------
r4 | ibro | 2005-06-07 19:05:49 +0300 (��, 07 ��� 2005) | 3 lines

command line parameters in sql-drus startup script


------------------------------------------------------------------------
r3 | ibro | 2005-06-07 19:05:18 +0300 (��, 07 ��� 2005) | 3 lines

Default readline set to GnuReadline for better line editing.
As first argument you can specify readline library name.

------------------------------------------------------------------------
r1 | ibro | 2005-06-07 17:59:52 +0300 (��, 07 ��� 2005) | 25 lines

Initial version of sql-drus - command line SQL utility for Informix in pure Java.
It uses readline library (lib/libreadline-java.jar), Informix JDBC type 4 driver 
from IBM (lib/ifxjdbc.jar) and log4j (lib/log4j.jar) for debug purposes.
Command overview:
 sql-drus [readline library] - default is ReadlineLibrary.PureJava
    CONN[ECT] {connectionAlias | full_jdbc_url} [user [password]]
    DISCONN[ECT]
    QUIT | EXIT
    DESC[RIBE] [owner.]{table|view|synonym}
    SPOOL {OFF | CONSOLE | file}
    SETVAR varName=value (to be implemented)
    / - execute last executed dml or ddl (not including sql-drus commands )
    <any_valid_ddl_or_dml_statement> ;
    HELP
Connection  aliases are specified in connection.properties in simple format
<alias>=<jdbc_url>
Commands are case insensitive. All DML and DDL shall be terminated by semicolon (;);
sql-drus commands shall NOT be terminated by semicolon (;)
Roadmap: Currently it uses ReadlineLibrary.PureJava, which very simple readline. 
         To be added ReadlineLibrary.GnuReadline
         Implement SETVAR command for easy variable sql binding in statements
         Implement @ command -> read commands from input file.
	 Implement proper behaviour for EXECUTE PROCEDURE, when it returns ResultSet


------------------------------------------------------------------------
